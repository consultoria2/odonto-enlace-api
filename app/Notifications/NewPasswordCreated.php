<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewPasswordCreated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $password)
    {
        //
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Has solicitado reiniciar tu contraseña en Odontoenlace')
                    ->greeting('Esta es tu nueva clave temporal')
                    ->line('¡Hola, ' . $this->user->name . '!')
                    ->line('Ingresa en <strong>odonto</strong><i>enlace</i> con esta contraseña y no olvides cambiarla en tu perfil de usuario.') 
                    ->line('Tu nueva contraseña es <strong>' . $this->password . '</strong>')
                    ->line('<strong>odonto</strong><i>enlace</i> en donde estés')
                    ->line('El cuidado de la salud oral nunca fue tan accesible');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
