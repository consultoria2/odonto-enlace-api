<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\TwilioChannel;
use App\Channels\Messages\TwilioMessage;
use Carbon\Carbon;

class AppointmentRemoved extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        //
        $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', TwilioChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('¡Bienvenido a Odontoenlace! - Tienes una cita cancelada')
                    ->greeting('¡Cita cancelada por Odontoenlace!')
                    ->line('¡Hola, ' . $this->appointment->patient_name . '!')
                    ->line($this->appointment->dentist->name . ' te ha cancelado la cita con los siguientes datos:')
                    ->line('Fecha y Hora: <strong>' . (new Carbon($this->appointment->appointment_date))->toDayDateTimeString() . '</strong>')
                    ->line('Lugar: <strong>' . $this->appointment->office->office_address . '</strong>')
                    ->line('<strong>odonto</strong><i>enlace</i> en donde estés')
                    ->line('El cuidado de la salud oral nunca fue tan accesible');
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioMessage)
            ->content($this->appointment->dentist->name . ' te ha cancelado una cita para el ' . 
                    (new Carbon($this->appointment->appointment_date))->toDayDateTimeString() . ' en ' .
                    $this->appointment->office->office_address );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
