<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class NewOrder extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order, $user, $store)
    {
        //
        $this->order = $order;
        $this->user = $user;
        $this->store = $store;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('¡Nuevo Pedido!')
                    ->greeting('¡Nuevo Pedido!')
                    ->line('¡Hola, ' . $this->user->name . '!')
                    ->line('Has creado el pedido #' . $this->order->store_order_number . ' para ' . $this->store->store_name . '. Podrás revisar los detalles en en <strong>odonto</strong><i>enlace</i>')
                    ->line('<strong>odonto</strong><i>enlace</i> en donde estés')
                    ->line('El cuidado de la salud oral nunca fue tan accesible');
    }

    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("¡Listo!")
            ->setBody('¡Hola, creste un nuevo pedido en ' . $this->store->store_name . '!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
