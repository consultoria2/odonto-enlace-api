<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class NewQualification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($qualification, $store, $order)
    {
        //
        $this->qualification = $qualification;
        $this->store = $store;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('¡Nueva Calificación Recibida!')
                    ->greeting('¡Te calificaron con ' . $this->qualification['score'] . ' ⭐s!')
                    ->line('¡Hola, ' . $this->store->store_name . '!')
                    ->line('Tienes una califificación de ' . $this->order->buyer->name . ' ' . $this->order->buyer->last_name . ' relacionada con el pedido' . $this->order->store_order_number . '. Podrás revisar los detalles en <strong>odonto</strong><i>enlace</i>')
                    ->line('Te han comentado lo siguiente "' . $this->qualification['title'] . ': ' . $this->qualification['message'] . '"')
                    ->line('<strong>odonto</strong><i>enlace</i> en donde estés')
                    ->line('El cuidado de la salud oral nunca fue tan accesible');
    }

    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("¡Nueva Calificación!")
            ->setBody('¡Hola, ' . $this->store->store_name . '! Tienes una nueva calificación de ' . $this->qualification['score'] . ' ⭐s!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
