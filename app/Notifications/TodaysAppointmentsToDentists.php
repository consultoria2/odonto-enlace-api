<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;

class TodaysAppointmentsToDentists extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointments, $dentist)
    {
        //
        $this->appointments = $appointments;
        $this->dentist = $dentist;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('¡Tus citas para hoy!')
                    ->greeting('¡Estas son tus citas para hoy!')
                    ->line('¡Hola, ' . $this->dentist->name . '!')
                    ->line('Tienes <strong>' . $this->appointments->citas . '</strong> para el día de hoy. Podrás revisar los detalles en en <strong>odonto</strong><i>enlace</i>')
                    ->action('Ver Agenda', 'https://odontoenlace.com/app#/assistant/schedule')
                    ->line('<small>Si tienes algún problema abriendo el enlace, copia y pega en el navegador el siguiente enlace:</small>')
                    ->line('<small>https://odontoenlace.com/app#/assistant/schedule</small>')
                    ->line('<strong>odonto</strong><i>enlace</i> en donde estés')
                    ->line('El cuidado de la salud oral nunca fue tan accesible');
    }

    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("¡Tus citas para hoy!")
            ->setBody('¡Hola, ' . $this->dentist->name . '! Tienes ' . $this->appointments->citas .' citas para hoy')
            ->setParameter('url', 'https://odontoenlace.com/app#/assistant/schedule');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
