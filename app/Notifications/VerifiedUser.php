<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifiedUser extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('¡Bienvenido a Odontoenlace! - Gracias por verificar tu cuenta')
                    ->greeting('¡Bienvenido a Odontoenlace!')
                    ->line('¡Hola, ' . $this->user->name . '!')
                    ->line('Gracias por verificar tu cuenta en <strong>odonto</strong><i>enlace</i>, la primera plataforma web diseñada exclusivamente para la odontología en Venezuela.') 
                    ->line('Con tu cuenta en <strong>odonto</strong><i>enlace</i> tendrás acceso a todo lo relacionado con el cuidado de la salud oral como nunca antes. Compra materiales, consigue y contrata servicios, mantente al día con la educación continua en odontología y mucho más, todo en la palma de tu mano.')
                    ->action('Ir a Odontoenlace', url('/'))
                    ->line('<strong>odonto</strong><i>enlace</i> en donde estés')
                    ->line('El cuidado de la salud oral nunca fue tan accesible');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
