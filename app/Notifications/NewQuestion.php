<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class NewQuestion extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $message, $product, $store)
    {
        //
        $this->user = $user;
        $this->store = $store;
        $this->message = $message;
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('¡Nueva Pregunta Recibida!')
                    ->greeting($this->user->name . ' te ha realizado una pregunta en tu tienda')
                    ->line('¡Hola, ' . $this->store->store_name . '!')
                    ->line('Tienes una pregunta de ' . $this->user->name . ' relacionada con el producto ' . $this->product->product_name . '. Podrás revisar los detalles en <strong>odonto</strong><i>enlace</i>')
                    ->action('Ver Pregunta', 'https://odontoenlace.com/app#/marketplace/questions')
                    ->line('<strong>odonto</strong><i>enlace</i> en donde estés')
                    ->line('El cuidado de la salud oral nunca fue tan accesible');
    }

    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("¡Nueva Pregunta!")
            ->setBody('¡Hola, ' . $this->store->store_name . '! Tienes una nueva pregunta de ' . $this->user->name . ' sobre el producto ' . $this->product->product_name)
            ->setParameter('url', 'https://odontoenlace.com/app#/marketplace/questions');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
