<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Store;

use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'last_name',
        'location',
        'phone',
        'profile_image',
        'player_id',
        'address',
        'state',
        'date_of_birth',
        'tax_id',
        'tax_id_letter',
        'status',
        'verification_token',
        'instagram_profile',
        'web_profile',
        'linkedin_profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The models that should be appended to each instance.
     *
     * @var array
     */
    protected $with = [
        'stores',
    ];

    protected $appends = [
        'user_roles',
        'graduate',
        'postgraduate',
        'certified',
        'courses',
        'offices'
    ];

    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    public function getGraduateAttribute(){
        $graduate = DB::table('users_institutes')->where('degree_name', 'Graduate')->where('user_id', $this->id)->get()->first();

        if(!is_null($graduate) && !is_null($graduate->degree_picture)){
            $graduate->degree_picture = env('APP_URL') . '/uploads/' . $graduate->degree_picture;
        }

        return $graduate;
    }

    public function getPostgraduateAttribute(){
        $postgraduates = DB::table('users_institutes')->where('degree_name', 'Postgraduate')->where('user_id', $this->id)->get();

        foreach($postgraduates as $postgraduate){
            if(!is_null($postgraduate->degree_picture)){
                $postgraduate->degree_picture = env('APP_URL') . '/uploads/' . $postgraduate->degree_picture;
            }
        }

        return $postgraduates;
    }

    public function getCertifiedAttribute(){
        $certificates = DB::table('users_institutes')->where('degree_name', 'Certificate')->where('user_id', $this->id)->get();

        foreach($certificates as $certificate){
            if(!is_null($certificate->degree_picture)){
                $certificate->degree_picture = env('APP_URL') . '/uploads/' . $certificate->degree_picture;
            }
        }

        return $certificates;
    }

    public function getCoursesAttribute(){
        $courses = DB::table('users_institutes')->where('degree_name', 'Course')->where('user_id', $this->id)->get();

        foreach($courses as $course){
            if(!is_null($course->degree_picture)){
                $course->degree_picture = env('APP_URL') . '/uploads/' . $course->degree_picture;
            }
        }

        return $courses;
    }

    public function getOfficesAttribute(){
        $offices = DB::table('offices')->where('user_id', $this->id)->where('is_active', true)->get();

        foreach($offices as $office){
            if(!is_null($office->office_schedule)){
                $office->office_schedule = json_decode($office->office_schedule);
            }
        }

        return $offices;
    }

    public function getUserRolesAttribute()
    {
        $roles = $this->getRoleNames();
        return $roles;
    }

    public function getProfileImageAttribute($value){
        if(is_null($value)){
            $image_url = env('APP_URL') . '/assets/img/profile_placeholder.png';
        } else {
            $image_url = env('APP_URL') . '/uploads/' . $value;
        }
        return $image_url;
    }

    public function routeNotificationForOneSignal()
    {
        return $this->player_id;
    }
}
