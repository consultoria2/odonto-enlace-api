<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use App\Models\Order;

use Illuminate\Notifications\Notifiable;

class Store extends Model
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['store_name', 'store_subdomain', 'store_description', 'store_address', 'store_email', 'store_phone', 'user_id', 'dirty', 'is_active', 'store_orders', 'store_qualifications', 'store_reputation', 'second_phone', 'allow_navigation'];


    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected $casts = [];

    /**
     * The attributes that should be appended.
     */
    protected $appends = [
        'logo',
        'unanswered_questions',
        'pending_orders',
        'categories',
        'qualifications',
        'owner',
    ];

    protected $with = [
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getLogoAttribute()
    {
        $store_setting = StoreSetting::where('store_id', $this->id)->where('setting_name', 'store_logo')->get()->first();

        $logo_url = '';

        if (is_null($store_setting)) {
            $logo_url = env('APP_URL') . '/assets/img/logo_perfil.png';
        } else {
            $logo_url = env('APP_URL') . '/uploads/' . $store_setting->setting_data;
        }

        return $logo_url;
    }

    public function getCategoriesAttribute()
    {
        if($this->id) {
            $product_categories = DB::select( DB::raw("select distinct x.category_id from (select pc.category_id from product_category pc inner join products p on p.id = pc.product_id where p.store_id = " . $this->id . " union all select category as category_id from products where store_id = " . $this->id  . " and category is not null) x"));
        }

        $categories = Array();

        foreach ($product_categories as $product_category){
            array_push($categories, Category::find($product_category->category_id));
        }

        return $categories;
    }

    public function getUnansweredQuestionsAttribute()
    {
        $unanswered = array();

        foreach ($this->products()->get() as $key => $product) {

            // Obtaining messages
            $messages = DB::table('questions_and_answers')->where('product_id', $product->id)->get();

            foreach ($messages as $message) {

                // Detect weather is a question
                if (is_null($message->parent_message)) {

                    // Detect if was answered
                    $was_answered = DB::table('questions_and_answers')->where('product_id', $product->id)->where('parent_message', $message->id)->first();

                    // If is was not respondend, add it to pending questions for the given product
                    if (is_null($was_answered)) {
                        array_push($unanswered, $message);
                    }
                }
            }
        }
        return count($unanswered);
    }

    public function getPendingOrdersAttribute()
    {
        $orders = $this->orders()->where('status', 'received')->get();
        return is_null($orders) ? 0 : $orders->count() ;
    }

    public function routeNotificationForMail($notification)
    {
        // Return email address only...
        return $this->store_email;

        // Return email address and name...
        return [$this->store_email => $this->store_name];
    }

    public function routeNotificationForOneSignal()
    {
        return $this->player_id;
    }

    public function getQualificationsAttribute()
    {
        $qualifications = DB::select( DB::raw("select * from qualifications where store_id = " . $this->id . " limit 2"));

        $response = Array();

        foreach($qualifications as $qualification)  {

            $user = User::find($qualification->created_by);
            unset($user->stores);
            $qualification->buyer = $user;
            
            array_push($response, $qualification);
        }

        return $response;
    }

    public function getOwnerAttribute()
    {
        $owner = User::find($this->user_id);
        unset($owner->stores);
        return $owner;
    }
}
