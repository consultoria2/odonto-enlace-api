<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Store;
use App\Models\OrderLines;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $fillable =   ['store_id', 'store_order_number', 'customer_name', 'customer_email', 'customer_phone', 'currency', 
                            'shipping_address', 'net_amount', 'tax_amount', 'total_amount', 'total_discount', 'total_lines', 'status', 'email_sent',
                            'comments', 'requires_invoice', 'created_by', 'used_exchange_rate', 'deliver_to', 'coupon_id'];

    protected $appends = [
        'qualification'
    ];

    protected $with = [
        'buyer'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function lines()
    {
        return $this->hasMany(OrderLines::class);
    }

    public function buyer(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getCreatedAtAttribute($value) {
        return (new Carbon($value))->toDateTimeString();
    }

    public function getQualificationAttribute($value) {
        $query = "select * from qualifications where order_id = ?";
        return DB::select($query, [$this->id]);
    }
}
