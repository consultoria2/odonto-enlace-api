<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['name', 'description', 'is_active', 'slide_image', 'priority', 'created_at', 'updated_at'];

    public function getSlideImageAttribute($value){
        if(is_null($value)){
            $image_url = env('APP_URL') . '/assets/img/logo_perfil.png';
        } else { 
            $image_url = env('APP_URL') . '/uploads/' . $value;
        }
        
        return $image_url;
    }
}
