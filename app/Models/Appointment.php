<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Appointment extends Model
{

    use Notifiable;

    protected $fillable = ['dentist_id', 'patient_id', 'patient_name', 'patient_email', 'patient_phone', 'office_id', 'notes', 'appointment_date', 'created_at', 'updated_at'];

    protected $appends = [
        'patient',
        'office',
        'dentist'
    ];

    public function getPatientAttribute()
    {
        if ($this->patient_id) {
            return User::find($this->patient_id);
        } else {
            return User::where('email', $this->patient_email)->get()->first();
        }
    }

    public function getDentistAttribute()
    {
        return User::find($this->dentist_id);
    }

    public function getOfficeAttribute()
    {
        return DB::table('offices')->where('id', $this->office_id)->get()->first();
    }

    public function routeNotificationForTwilio()
    {
        return $this->patient_phone;
    }

    public function routeNotificationForMail($notification)
    {
        // Return email address only...
        return $this->patient_email;
    }
}
