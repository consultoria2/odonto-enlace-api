<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Store;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use App\Models\ExchangeRate;
use App\Models\Tax;

class Product extends Model
{
    protected $fillable = ['product_type', 'main_image', 'thumb_image', 'product_name', 'brand', 'discount_price', 'price', 'avg_rate', 'availability', 'product_code', 'tags', 'description', 'features'. 'image_gallery', 'category', 'stock', 'store_id', 'tax_id', 'source', 'dirty', 'is_active', 'starred', 'archived'];

    protected $with = ['store', 'tax'];

    protected $appends = ['questions_and_answers','image_objects', 'price_ves', 'discount_price_ves', 'categories', 'favorite', 'remaining', 'variations'];
    
    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function getMainImageAttribute($value){
        if(is_null($value)){
            $image_url = env('APP_URL') . '/assets/img/logo_perfil.png';
        } else if ($value == '*'){
            $image = DB::table('product_images')->where('product_id', $this->id)->first();
            if(!is_null($image)){
                $image_url = env('APP_URL') . '/uploads/' . $image->image;
            } else {
                $image_url = env('APP_URL') . '/assets/img/logo_perfil.png';
            }
        } else { 
            $image_url = env('APP_URL') . '/uploads/' . $value;
        }
        
        return $image_url;
    }

    public function getImageGalleryAttribute($value){
        if (is_null($value)){
            $images = DB::table('product_images')->where('product_id', $this->id)->get();
        }

        return array_map(function($image) { return env('APP_URL') . '/uploads/' . $image; }, $images->pluck('image')->toArray());
    }

    public function getQuestionsAndAnswersAttribute(){
        // return DB::table('questions_and_answers')->where('product_id', $this->id)->orderBy('id', 'ASC')->orderBy('parent_message', 'ASC')->get();
        $query = "select * from (select least(id, coalesce(parent_message, 2147483647)) as position, s.* from questions_and_answers s where product_id = ?) x order by x.position desc, x.id";
        return DB::select($query, [$this->id]);
    }

    public function getImageObjectsAttribute(){
        return DB::table('product_images')->where('product_id', $this->id)->get();
    }

    public function getPriceVesAttribute(){
        return (new ExchangeRate([], 'USD', 'VES'))->convert($this->price);
    }

    public function getDiscountPriceVesAttribute(){
        return (new ExchangeRate([], 'USD', 'VES'))->convert($this->discount_price);
    }

    public function getCategoriesAttribute(){
        $categories = DB::table('product_category')->where('product_id', $this->id)->get()->pluck('category_id')->toArray();
        
        if($this->category){
            array_push($categories, $this->category);
        }

        return array_unique($categories);
    }

    public function getVariationsAttribute(){
        $variations = DB::table('product_variation')->where('product_id', $this->id)->get()->pluck('variation')->unique()->toArray();

        return array_values($variations);
    }

    public function getFavoriteAttribute(){
        $favorite = null;

        if(auth()->user() && auth()->user()->id){
            $favorite = DB::table('favorites')->where(['user_id' => auth()->user()->id, 'product_id' => $this->id])->first();
        }
        
        return !is_null($favorite);
    }

    public function getRemainingAttribute(){
        $query = "select coalesce(sum(qty)) as total_units from order_lines where product_id = ? and processed = false";
        $remaining = $this->stock - DB::select($query, [$this->id])[0]->total_units;
        return $remaining;
    }
}
