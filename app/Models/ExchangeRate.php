<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExchangeRate extends Model
{
    // We override the constructor to set if given the default 'from' 
    // and 'to' currencies values
    public function __construct($attributes = array(), $from = null, $to = null)
    {
        parent::__construct($attributes);

        if($from && $to)
        {
            $this->from_currency = $from;
            $this->to_currency = $to;
        }
    }

    protected $table = 'exchange_rates';

    protected $fillable = ['from_currency', 'to_currency', 'rate', 'since', 'until', 'created_at', 'updated_at', 'deleted_at', 'fixed', 'edited'];

    public function getDateFormat()
    {
        return 'Y-m-d H:i:s';
    }

    public function lastExchange($from = null, $to = null, $by = null){
        // Retrieving the last record for the given from and to values set when 
        // the model was instantiated

        // If a top date is set, we retrieve the most recent value by the given date
        if($by){
            return self::where('from_currency', $from ? $from : $this->from_currency)
                ->where('to_currency', $to ? $to : $this->to_currency)
                ->where('until', '<=', $by)
                ->orderBy('until', 'DESC')
                ->get()->first();
        } else {
            return $this->currentExchange($from, $to);
        }
    }

    public function currentExchange($from = null, $to = null){
        // Retrieving the last record for the given from and to values set when 
        // the model was instantiated

        // Trying to find a fixed value first
        $fixed = self::where('from_currency', $from ? $from : $this->from_currency)
                ->where('to_currency', $to ? $to : $this->to_currency)
                ->where('fixed', true)
                ->get()->first();

        if(!is_null($fixed)){
            return $fixed;
        }
        
        // Else, we retrive the current stored value
        return self::where('from_currency', $from ? $from : $this->from_currency)
            ->where('to_currency', $to ? $to : $this->to_currency)
            ->whereNull('until')
            ->get()->first();
    }    

    // A method for direct exchange rate calculation
    public function convert($value, $from = null, $to = null, $by = null){
        if($by){
            return $this->lastExchange($by, $from, $to)->rate * $value;
        } else {
            return $this->currentExchange($from, $to)->rate * $value;
        }
    }

    public function getRateAttribute($value){
        return round($value, 2);
    }
}
