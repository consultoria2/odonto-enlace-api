<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Store;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class RunStoresStatistics extends Command
{

    protected $signature = 'stores:statistics {store?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update the stores' statistics";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('store')){
            $stores = Store::where('store_subdomain', $this->argument('store'))->get()->first();
        } else {
            $stores = Store::all();
        }

        $this->info(Carbon::now() . 
                    ' Stores to update their statistics ' . count($stores));

        foreach($stores as $store){

            $this->info(Carbon::now() . 
                    ' Processing ' . $store->store_name . '...');

            $orders = Order::where('store_id', $store->id)->where('status', 'delivered')->get();

            $store->store_orders = $orders->count();

            $this->info(Carbon::now() . 
                ' Completed orders for ' . $store->store_name . ': ' . $orders->count());

            $qualifications = DB::select( DB::raw("select coalesce(count(*), 0) as store_qualifications, coalesce(avg(score), 0) as store_reputation from qualifications where store_id = " . $store->id));
        
            foreach ($qualifications as $qualification){
                $store->store_qualifications = $qualification->store_qualifications;

                $this->info(Carbon::now() . 
                ' Qualifications for ' . $store->store_name . ': ' . $qualification->store_qualifications);

                $store->store_reputation = $qualification->store_reputation;

                $this->info(Carbon::now() . 
                ' Reputation for ' . $store->store_name . ': ' . $store->store_reputation);
            }
        
            $store->save();

            $this->info(Carbon::now() . 
                    ' Finished processing ' . $store->store_name);
        }  
    }
}

