<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Notifications\AppointmentReminding;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class NotifyTomorrowAppointmentsToPatients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:patient-appointments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a sms message to every patient who has an appointment tomorrow';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $appointments = Appointment::whereRaw("date(appointment_date) > date(CURDATE()) and date(appointment_date) <= date(date_add(CURDATE(), interval 1 day)) and length(patient_phone) = 13")->get();
        
        foreach($appointments as $appointment){
            $appointment->notify(new AppointmentReminding($appointment));
            $this->info('Notifying to: ' . $appointment->patient_name . ' ' . $appointment->appointment_date);
        }
        
        if(count($appointments) > 0){
            $this->info('Notifications of tomorrows appointments had been sent');
        } else {
            $this->info('There are not appointments for tomorrow, '. Carbon::now()->addDays(1)->format('Y-m-d'));
        }
        
        return 0;
    }
}
