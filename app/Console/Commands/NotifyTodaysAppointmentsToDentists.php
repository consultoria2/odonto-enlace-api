<?php

namespace App\Console\Commands;

use App\Notifications\TodaysAppointmentsToDentists;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class NotifyTodaysAppointmentsToDentists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:dentist-appointments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a push message to every user who has an appointment today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $appointments = DB::table('appointments')
             ->select(DB::raw('count(*) as citas, dentist_id, date(appointment_date)'))
             ->whereRaw("date(appointment_date) = '" . Carbon::now()->format('Y-m-d') . "'")
             ->groupByRaw("dentist_id, date(appointment_date)")
             ->get();
        
        foreach($appointments as $appointment){
            $dentist = User::find($appointment->dentist_id);
            $dentist->notify(new TodaysAppointmentsToDentists($appointment, $dentist));
            $this->info('Notifying to: ' . $dentist->name);
        }
        
        if(count($appointments) > 0){
            $this->info('Notifications of todays appointments had been sent');
        } else {
            $this->info('There are not appointments for today, '. Carbon::now()->format('Y-m-d'));
        }
        
        return 0;
    }
}
