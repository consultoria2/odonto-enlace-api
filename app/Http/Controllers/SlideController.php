<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slide;
use App\Models\Store;
use ImageOptimizer;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DB;

class SlideController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $slides = Slide::orderBy('priority')->get();
        return view('slides.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = Store::where('is_active', true)->get()->map(function ($store) {
            return [$store->id => $store->store_name];
        });

        $stores = $stores->toArray();

        return view('slides.create', compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'slide_image' => 'required',
            'store_id' => 'required',
        ]);

        $element = $request->all();

        // Validate if the slide image is received

        $extension = $request->file('slide_image')->getClientOriginalExtension();
        $folderPath = "images/" . $element['store_id'] . "/";
        $file = $folderPath . uniqid() . '.' . $extension;

        if (Storage::disk('public_uploads')->put($file, $request->file('slide_image')->get())) {

            // Resize if image is too wide
            $img = Image::make(public_path() . '/uploads/' . $file);

            if ($img->width() > 1200) {
                $img->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $img->save(public_path() . '/uploads/' . $file);
            }

            // Optimize image
            ImageOptimizer::optimize(public_path() . '/uploads/' . $file);

            $element['slide_image'] = $file;
        } else {
            $response = [
                'error' => 'File upload failed',
                'message' => 'We could not upload your file'
            ];
            return response()->json($response, 401);
        }

        $element['is_active'] = true;
        $element['created_at'] = Carbon::now();
        $element['updated_at'] = Carbon::now();

        $slide = Slide::create($element);

        return redirect()->route('slides.index')
            ->with('success', 'Slide creado satisfactoriamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slides::find($id);
        return view('slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'slide_image' => 'required',
        ]);

        $slide = Slide::find($id);

        $element = $request->all();

        $slide->fill($element);
        $slide->save();

        return redirect()->route('slides.index')
            ->with('success', 'Slide actualizado satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("slides")->where('id', $id)->delete();
        return redirect()->route('slides.index')
            ->with('success', 'Slide eliminado satisfactoriamente');
    }

    /**
     * Suspend the specified Slide.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function suspend($id)
    {
        $slide = Slide::find($id);
        $slide->is_active = false;
        $slide->save();

        return redirect()->route('slides.index')
            ->with('success', 'Slide suspendido satisfactoriamente');
    }

    /**
     * Activate the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $slide = Slide::find($id);
        $slide->is_active = true;
        $slide->save();

        return redirect()->route('slides.index')
            ->with('success', 'Slide activado satisfactoriamente');
    }
}
