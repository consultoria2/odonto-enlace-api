<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use DB;
use Faker\Provider\ar_SA\Payment;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payment_methods = PaymentMethod::orderBy('id', 'DESC')->paginate(10);
        return view('payment-methods.index', compact('payment_methods'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('payment-methods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'currency' => 'required',
            'name' => 'required',
            'instructions' => 'required'
        ]);

        $payment_method = PaymentMethod::create($request->all());

        return redirect()->route('payment-methods.index')
            ->with('success', 'Forma de pago creada satisfactoriamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment_method = PaymentMethod::find($id);
        return view('payment-methods.edit', compact('payment_method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'currency' => 'required',
            'name' => 'required',
            'instructions' => 'required'
        ]);

        $payment_method = PaymentMethod::find($id);
        $payment_method->fill($request->all());
        $payment_method->save();

        return redirect()->route('payment-methods.index')
            ->with('success', 'Forma de pago actualizada satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id', $id)->delete();
        return redirect()->route('roles.index')
            ->with('success', 'Rol eliminado satisfactoriamente');
    }

    /**
     * Suspend the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function suspend($id)
    {
        $payment_method = PaymentMethod::find($id);
        $payment_method->is_active = false;
        $payment_method->save();

        return redirect()->route('payment-methods.index')
            ->with('success', 'Forma de pago suspendida satisfactoriamente');
    }

    /**
     * Activate the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $payment_method = PaymentMethod::find($id);
        $payment_method->is_active = true;
        $payment_method->save();

        return redirect()->route('payment-methods.index')
            ->with('success', 'Forma de pago activada satisfactoriamente');
    }      
}
