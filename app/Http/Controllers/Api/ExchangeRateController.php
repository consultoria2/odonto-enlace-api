<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ExchangeRate;

class ExchangeRateController extends Controller
{
    public function list($store)
    {
    }
    
    public function getCurrentExchange()
    {
        $exchange_rate = (new ExchangeRate([], 'USD', 'VES'))->convert(1);
        return response()->json($exchange_rate, 200);
    }
}