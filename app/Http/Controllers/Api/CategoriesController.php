<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Store;
use App\Models\Product;


class CategoriesController extends Controller
{
    public function list()
    {
        $categories = Category::where('is_active', true)->get();
        return response()->json($categories, 200);
    }

    // public function storeList($store)
    // {
    //     $store = Store::where('store_subdomain', $store)->where('user_id', auth()->user()->id)->first();

    //     if(is_null($store)){
    //         $response = [
    //             'error' => 'Not authorized',
    //             'message' => 'This action has been logged and you may be suspended'
    //         ];
    //         return response()->json($response, 401);
    //     }

    //     $product_categories = Product::where('store_id', $store->id)->get()->pluck('category');

    //     $categories = Category::where('is_active', true)->whereIn('id', $product_categories)->get();

    //     return response()->json($categories, 200);
    // }
}
