<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\DB;

use App\Models\Store;
use App\Models\StoreSetting;
use App\Models\Product;
use App\Models\Tax;
use App\Models\User;

use App\Models\Order;
use App\Models\OrderLines;
use App\Notifications\NewOrder;
use App\Notifications\NewQualification;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Artisan;

class OrderController extends Controller
{
    public function list($store)
    {
        $store_id = Store::where('store_subdomain', $store)->first()->id;
        $orders = Order::where('store_id', $store_id)->get();
        return $orders;
    }

    public function mine()
    {
        $orders = Order::where('created_by', auth()->user()->id)->with('store')->orderBy('created_at', 'DESC')->get();
        return response()->json($orders, 200);
    }

    public function find($id)
    {
        $order = Order::where('id', $id)
            ->with('store')
            ->with('lines')
            ->get()->first();
        return response()->json($order, 200);
    }    

    public function store(Request $request)
    {
        $request->validate([
            'customer' => 'required',
            'products' => 'required',
            'shipping_address' => 'required',
            'currency' => 'required',
            'used_exchange_rate' => 'required|numeric'
        ]);

        $input = $request->all();

        //---------------------------------------------------------------
        //-- Separating lines by store
        //---------------------------------------------------------------        

        $orders = Array();

        $aux_products = $request->input('products.*');

        $products = Array();

        foreach($aux_products as $product){
            array_push($products, json_decode($product));
        }

        foreach($products as $line) {
            if (!array_key_exists($line->store_id, $orders)) {
                $orders[$line->store_id] = Array();
            }
            array_push($orders[$line->store_id], $line);
        }

        //---------------------------------------------------------------
        //-- Preparing transaction
        //---------------------------------------------------------------

        $saved_orders = Array();

        DB::beginTransaction();

        foreach($orders as $key => $lines) {

        //---------------------------------------------------------------
        //-- Retrieving store
        //---------------------------------------------------------------
            $store = Store::find($key);
            $order = Array();
            
            if(is_null($store)){
                $response = [
                    'error' => 'Not found',
                    'code' => 404,
                    'message' => 'Store ' . $key . ' not found'
                ];
                throw new \Exception($response);
            }

            $order['store_id'] = $store->id;

        //---------------------------------------------------------------
        //-- Retrieving store order number
        //---------------------------------------------------------------

            $setting = StoreSetting::where('setting_name', 'next_order_number')->where('store_id', $store->id)->get();
            $next_order_number = 1;

            if($setting->count()){
                $setting = $setting->first();
                $next_order_number = $setting->setting_data;
                $next_order_number++;
                $setting->setting_data = $next_order_number;
                $setting->save();
            } else {
                $setting = [];
                $setting['store_id'] = $store->id;
                $setting['setting_name'] = 'next_order_number';
                $setting['setting_data'] = $next_order_number;
                StoreSetting::create($setting);
            }

            $order['store_order_number'] = $next_order_number;

        //---------------------------------------------------------------
        //-- Setting order customer data
        //---------------------------------------------------------------
            $customer = User::find($input['customer']);

            $order['customer_name'] = $customer->name;
            $order['customer_email'] = $customer->email;
            $order['customer_phone'] = $customer->phone;            
        

        //---------------------------------------------------------------
        //-- Setting order related data
        //---------------------------------------------------------------

            $order['shipping_address'] = $input['shipping_address'];
            $order['currency'] = $input['currency'];
            $order['comments'] = $input['comments'];
            $order['deliver_to'] = $input['deliver_to'];
            // $order['requires_invoice'] = $input['requires_invoice'];
            $order['created_by'] = auth()->user()->id;
            $order['store_id'] = $store->id;
            $order['net_amount'] = 0;
            $order['tax_amount'] = 0;
            $order['total_amount'] = 0;
            $order['total_lines'] = 0;
            $order['used_exchange_rate'] = $input['used_exchange_rate'];

            if (isset($input['coupon_id'])){
                $order['coupon_id'] = $input['coupon_id'];
            }
            
            $order['total_discount'] = 0;

        //---------------------------------------------------------------
        //-- Saving order's header
        //---------------------------------------------------------------

            $order = Order::create($order);
            $order->net_amount = 0;
            $order->tax_amount = 0;
            $order->total_amount = 0;
            $order->total_lines = 0;
            $order->total_discount = 0;

        //---------------------------------------------------------------
        //-- Retrieving coupon value
        //---------------------------------------------------------------

        $coupon = null;

        if (isset($input['coupon_id'])){
            $coupon = Coupon::where('is_active', true)->where('id', $input['coupon_id'])->get()->first();

            if (is_null($coupon)){
                $response = [
                    'error' => 'Not found',
                    'code' => 404,
                    'message' => 'Coupon not found'
                ];
                DB::rollBack();
                return response()->json($response, $response['code']);
            }
        }
        

        //---------------------------------------------------------------
        //-- Saving order's lines
        //---------------------------------------------------------------

            foreach($lines as $line){
                $product = Product::where('id', $line->id)->where('store_id', $store->id)->get()->first();

                if($product){
                    $order_line['order_id'] = $order->id;
                    $order_line['product_id'] = $line->id;
                    $order_line['qty'] = intval($line->qty);

                    if($line->discount_price && floatval($line->discount_price) < floatval($line->price)){
                        $order_line['net_price'] = floatval($line->discount_price);
                    } else {
                        $order_line['net_price'] = floatval($line->price);
                    }

                    
                    $order_line['net_amount'] = $order_line['qty'] * $order_line['net_price'];

                    if(!is_null($coupon)){
                        $order_line['discount_amount'] = $order_line['net_amount'] * $coupon->coupon_value;
                    } else {
                        $order_line['discount_amount'] = 0;
                    }

                    if (isset($line->selected_variation)){
                        $order_line['comments'] = $line->selected_variation;
                    }
                    
                    $tax = $product->tax;

                    if($tax){
                        $order_line['tax_amount'] = ($order_line['net_amount'] - $order_line['discount_amount']) * $tax->tax_value ;
                    } else {
                        $response = [
                            'error' => 'Not found',
                            'code' => 404,
                            'message' => 'Tax for product ' . $product->product_name . ' not found'
                        ];
                        DB::rollBack();
                        return response()->json($response, $response['code']);
                    }
                    OrderLines::create($order_line);
                    
                    $order->net_amount += $order_line['net_amount'];
                    $order->tax_amount += $order_line['tax_amount'];
                    $order->total_discount += $order_line['discount_amount'];
                    $order->total_amount += ($order_line['net_amount'] - $order_line['discount_amount'] + $order_line['tax_amount']);
                    $order->total_lines++;
                } else {
                    $response = [
                        'error' => 'Not found',
                        'code' => 404,
                        'message' => 'Product (' . $line->id . ' - ' . $line->product_name . ') not found in the store ' . $store->store_name
                    ];
                    DB::rollBack();
                    return response()->json($response, $response['code']);
                }
            }

            $order->status = 'received';
            $order->save();

            array_push($saved_orders, $order);
        }

        DB::commit();

        foreach($saved_orders as $key => $order) {
            $store = Store::find($order->store_id);           
            $user = User::find(auth()->user()->id);
            
            try {
                $user->notify(new NewOrder($order, $user, $store));
            } catch (Exception $e) {

            }

            try {
                $odontoenlace = User::where('email', 'odontoenlace.cuentas@gmail.com')->get()->first();
                if(!is_null($odontoenlace)){
                    $odontoenlace->notify(new NewOrder($order, $user, $store));
                }
            } catch (Exception $e) {

            }
            
        }

        return response()->json($orders, 200);
    }

    public function performQualification(Request $request){

        $request->validate([
            'order_id' => 'required|exists:orders,id',
            'title' => 'required',
            'message' => 'required',
            'score' => 'required|numeric|between:0,5',
        ]);

        $qualification = $request->all();

        $order = Order::find($qualification['order_id']);

        $row = DB::table('qualifications')->insert(
            ['store_id' => $order->store_id, 
            'order_id' => $order->id,
            'title' => $qualification['title'],
            'message' => $qualification['message'],
            'score' => $qualification['score'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'created_by' => auth()->user()->id]
        );

        Artisan::call('stores:statistics');

        $store = Store::find($order->store_id);

        if (!is_null($store)){
            try {
                $store->notify(new NewQualification($qualification, $store, $order));
            } catch (Exception $e) {

            }
        }

        return $row;
    }

    public function updateStatus(Request $request){

        $request->validate([
            'order_id' => 'required|exists:orders,id',
            'status' => 'required',
        ]);

        $status = $request->all();

        $order = Order::find($status['order_id']);
        $order->status = $status['status'];
        $order->save();

        return $order;
    }

    public function getAvailableCoupons(){
        $coupons = Coupon::where('is_active', true)->where('until', '>=', Carbon::now())->where('since', '<=', Carbon::now())->get();

        foreach($coupons as $key => $coupon){
            $order = Order::where('created_by', auth()->user()->id)->where('status', '<>', 'cancelled')->where('coupon_id', $coupon->id)->get()->first();
            if (!is_null($order)){
                $coupons->forget($key);
            }
        }

        return $coupons;        
    }
}
