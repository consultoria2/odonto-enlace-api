<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Store;
use App\Models\Product;
use App\Models\Tax;
use Illuminate\Support\Facades\DB;
use ImageOptimizer;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function list($store, $limit = 0)
    {
        $store_id = $store;

        if ($limit > 0) {
            $products = Product::where('store_id', $store_id)->where('availability', true)->get()->shuffle()->take($limit);
        } else {
            $products = Product::where('store_id', $store_id)->get();
        }

        


        return response()->json($products, 200);
    }

    public function find($id)
    {
        $product = Product::find($id);

        if (!$product) {
            $response = [
                'error' => 'Not found',
                'message' => 'The product was not found',
                'type' => 'info'
            ];

            return response()->json($response, 404);
        }

        return response()->json($product, 200);
    }

    public function listPublicOffers($limit = 0)
    {
        return $this->listOffers(null, $limit);
    }

    public function listStoreOffers($store, $limit = 0)
    {
        return $this->listOffers($store, $limit);
    }

    public function listOffers($store, $limit = 0)
    {
        $pivot = 0;

        if ($store) {
            $store = Store::where('store_subdomain', $store)->get()->first();
            $products = Product::where('availability', true)->where('archived', false)
                ->where('store_id', $store->id);
        } else {
            $products = Product::where('availability', true)->where('archived', false);
        }

        //Randomly order the result
        $products = $products->get()->shuffle();

        $arr_products = array();

        foreach ($products as $key => $product) {
            if ($product->discount_price && (floatval($product->discount_price) < floatval($product->price)) && $product->store->is_active == true) {

                unset($product->questions_and_answers);
                unset($product->categories);

                array_push($arr_products, $product);
                $pivot++;
            } else {
                $products->forget($key);
            }

            if ($limit > 0 && $limit == $pivot) break;
        }
        return response()->json($arr_products, 200);
    }

    public function search($query)
    {
        $arr_products = Array();
        
        $stores = Store::where('is_active', true)->get()->pluck('id');

        if ($query == 'destacados') {
            $products = Product::where('availability', true)->where('archived', false)->where('starred', true)->whereIn('store_id', $stores)->get()->shuffle();
        } else {
            $products = Product::whereRaw("UPPER(product_name) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("UPPER(description) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("id IN (SELECT p.product_id FROM product_category p WHERE p.category_id = (SELECT c.id FROM categories c WHERE UPPER(c.category_name) LIKE '%" . strtoupper($query) . "%' and c.is_active = 1))")->get();

            foreach($products as $key => $product){
                if(!$product->store->is_active){
                    $products->forget($key);
                } else {
                    array_push($arr_products, $product);
                }
            }
        }
        
        return response()->json(count($arr_products) > 0 ? $arr_products : $products, 200);
    }

    public function storeSearch($store, $query, $limit = 0)
    {
        $store = Store::where('store_subdomain', $store)->get()->first();

        if ($query == "*") {
            $products = Product::whereRaw("store_id = " . $store->id)->where('availability', true)->where('archived', false)->get();
        } else {
            $products = Product::whereRaw("store_id = " . $store->id . " and UPPER(product_name) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("store_id = " . $store->id . " and UPPER(description) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("store_id = " . $store->id . " and id IN (SELECT p.product_id FROM product_category p WHERE p.category_id in (SELECT c.id FROM categories c WHERE UPPER(c.category_name) LIKE '%" . strtoupper($query) . "%' and c.is_active = 1))")->get();
        }

        if ($limit > 0){
            $category_count = Array();
            foreach($products as $key => $product) {

                foreach($product->categories as $category_id){
                    if (array_key_exists($category_id, $category_count)) {
                        $category_count[$category_id] = $category_count[$category_id] + 1;
                    } else {
                        $category_count[$category_id] = 0;
                    }

                    if($category_count[$category_id] >= $limit){
                        $products->forget($key);
                    }
                }     
            }
        }

        return response()->json($products, 200);
    }

    public function storeCategorySearch($store, $query, $category)
    {
        $store = Store::where('store_subdomain', $store)->get()->first();

        if ($query == "*") {
            $products = Product::whereRaw("store_id = " . $store->id)->where('availability', true)->where('archived', false)->get();
        } else {
            $products = Product::whereRaw("store_id = " . $store->id . " and UPPER(product_name) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("store_id = " . $store->id . " and UPPER(description) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")->get();
        }

        $products = $products->filter(function ($value) use ($category) {
            return in_array($category, $value->categories);
        });

        foreach ($products as $product) {
            $product->category = $category;
        }

        return response()->json($products, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required|string',
            'price' => 'required|numeric',
            'availability' => 'required',
            'stock' => 'required|numeric',
            'brand' => 'required|string',
            'description' => 'required|string',
            'product_code' => 'required|string',
            'product_category' => 'required'
        ]);

        $product_data = $request->all();

        if (!$request->store_id || $request->store_id == 'null' || $request->store_id == 'undefined') {
            $store = Store::where('user_id', auth()->user()->id)->first();
        } else {
            $store = Store::where('id', $request['store_id'])->where('user_id', auth()->user()->id)->first();
        }

        if (is_null($store)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }


        if (!$request->product_code || $request->product_code == 'null' || $request->product_code == 'undefined') {
            $product_data['product_code'] = $request->product_name;
        }

        if (!$request->discount_price || $request->discount_price == 'null' || $request->discount_price == 'undefined') {
            $product_data['discount_price'] = $request->price;
        }

        $product_images = $request->input('product_images.*');

        if ($product_images && count($product_images) > 0) {
            $product_data['main_image'] = '*';
        }

        $product_data['tax_id'] = !is_null($request->tax_id) ? $request->tax_id : env('DEFAULT_TAX_ID', 0);

        $product = Product::create($product_data);

        // Validate product images

        if ($product_images && count($product_images) > 0) {

            foreach ($product_images as $file) {
                $folderPath = "images/" . $store->id . "/";
                $image_parts = explode(";base64,", $file);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $name = $folderPath . uniqid() . '.' . $image_type;

                if (Storage::disk('public_uploads')->put($name, $image_base64)) {

                    // Resize if image is too wide
                    $img = Image::make(public_path() . '/uploads/' . $name);

                    if ($img->width() > 1200) {
                        $img->resize(1200, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
    
                        $img->save(public_path() . '/uploads/' . $name);
                    }
                    

                    // Optimize image
                    ImageOptimizer::optimize(public_path() . '/uploads/' . $name);
                    DB::table('product_images')->insert(['product_id' => $product->id, 'image' => $name]);

                    if ($product_data['main_image']  == '*') {
                        $product_data['main_image']  = $name;
                        $product->main_image = $name;
                        $product->save();
                    }
                } else {
                    $response = [
                        'error' => 'File upload failed',
                        'message' => 'We could not upload your file'
                    ];
                    return response()->json($response, 401);
                }
            }
        }

        $product_categories = $request->input('product_category.*');

        foreach ($product_categories as $category) {
            DB::table('product_category')->insert(['product_id' => $product->id, 'category_id' => $category]);
        }

        $product_variations = $request->input('product_variation.*');

        DB::table('product_variation')->where('product_id', $product->id)->delete();

        if ($product_variations) {
            foreach ($product_variations as $variation) {
                DB::table('product_variation')->insert(['product_id' => $product->id, 'variation' => $variation]);
            }
        }


        return response()->json($product, 200);
    }

    public function edit(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'product_name' => 'required|string',
            'price' => 'required|numeric',
            'availability' => 'required',
            'stock' => 'required|numeric',
            'brand' => 'required|string',
            'description' => 'required|string',
            'product_code' => 'required|string',
            'product_category' => 'required'
        ]);

        $product_data = $request->all();

        if (!$request->store_id || $request->store_id == 'null' || $request->store_id == 'undefined') {
            $store = Store::where('user_id', auth()->user()->id)->first();
        } else {
            $store = Store::where('id', $request['store_id'])->where('user_id', auth()->user()->id)->first();
        }

        if (is_null($store)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }


        if (!$request->product_code || $request->product_code == 'null' || $request->product_code == 'undefined') {
            $product_data['product_code'] = $request->product_name;
        }

        if (!$request->discount_price || $request->discount_price == 'null' || $request->discount_price == 'undefined') {
            $product_data['discount_price'] = $request->price;
        }

        $product_images = $request->input('product_images.*');

        if ($product_images && count($product_images) > 0) {
            $product_data['main_image'] = '*';
        } else {
            $product_images = array();
        }

        $product_data['tax_id'] = !is_null($request->tax_id) ? $request->tax_id : env('DEFAULT_TAX_ID', 0);

        $product = Product::find($product_data['id']);

        $product->fill($product_data);

        $product->save();

        // Validate product images

        if (count($product_images) > 0) {

            foreach ($product_images as $file) {
                $folderPath = "images/" . $store->id . "/";
                $image_parts = explode(";base64,", $file);

                // Validate if is base64 encoded
                if (count($image_parts) > 1) {

                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                    $image_base64 = base64_decode($image_parts[1]);
                    $name = $folderPath . uniqid() . '.' . $image_type;

                    if (Storage::disk('public_uploads')->put($name, $image_base64)) {

                        DB::table('product_images')->insert(['product_id' => $product->id, 'image' => $name]);

                        if ($product_data['main_image']  == '*') {
                            $product_data['main_image']  = $name;
                            $product->main_image = $name;
                            $product->save();
                        }
                    } else {
                        $response = [
                            'error' => 'File upload failed',
                            'message' => 'We could not upload your file'
                        ];
                        return response()->json($response, 401);
                    }
                }
            }
        }

        $product_categories = $request->input('product_category.*');

        DB::table('product_category')->where('product_id', $product->id)->delete();

        foreach ($product_categories as $category) {
            DB::table('product_category')->insert(['product_id' => $product->id, 'category_id' => $category]);
        }

        $product_variations = $request->input('product_variation.*');

        DB::table('product_variation')->where('product_id', $product->id)->delete();

        if ($product_variations) {
            foreach ($product_variations as $variation) {
                DB::table('product_variation')->insert(['product_id' => $product->id, 'variation' => $variation]);
            }
        }

        if ($request->get('reset_stock') && $request->get('reset_stock') == true) {
            DB::table('order_lines')->where('product_id', $product->id)->update(['processed' => true]);
        }

        return response()->json($product, 200);
    }

    public function view($store, $id)
    {
        $store_id = Store::where('store_subdomain', $store)->first()->id;
        $product = Product::where('store_id', $store_id)->where('id', $id)->first();
        // $product->main_image = env('APP_URL') . '/public/' . $product->main_image;
        return $product;
    }

    public function delete(Request $request)
    {
        $request->validate([
            'store_id' => 'required|numeric',
            'id' => 'required|numeric',
        ]);

        // Validate authorized hacked access
        $store = Store::where('id', $request['store_id'])->where('user_id', auth()->user()->id)->first();

        if (is_null($store)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $orders = DB::table('order_lines')->where('product_id', $request->id)->get()->first();

        if (!is_null($orders)) {
            $response = [
                'error' => 'Forbidden',
                'message' => 'The product exists in other orders'
            ];

            return response()->json($response, 403);
        }

        $store_id = $store->id;

        $categories = DB::table('product_category')->where(['product_id' => $request->id])->delete();

        $deleted = Product::where('store_id', $store_id)->where('id', $request->id)->delete();

        if (!$deleted) {
            $response = [
                'error' => 'Not found',
                'message' => 'The product was not found',
                'type' => 'info'
            ];

            return response()->json($response, 404);
        }

        $response = [
            'success' => 'Action performed',
            'message' => 'Row count: ' . $deleted
        ];

        return response()->json($response, 200);
    }

    public function removePicture(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'product_id' => 'required|numeric',
            'image' => 'required|string'
        ]);

        $image_data = $request->all();

        Storage::disk('public_uploads')->delete($image_data['image']);

        $response = DB::table('product_images')->where(['id' => $image_data['id'], 'product_id' => $image_data['product_id']])->delete();

        return response()->json($response, 200);
    }

    public function addToFavorite(Request $request)
    {
        $request->validate([
            'product_id' => 'required|numeric',
        ]);

        $product = Product::find($request->product_id);

        if (is_null($product)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $exists = DB::table('favorites')->where('product_id', $product->id)->where('user_id', auth()->user()->id)->get()->first();

        if (is_null($exists)) {
            DB::table('favorites')->insert(['product_id' => $product->id, 'user_id' => auth()->user()->id]);
        } else {
            $response = [
                'error' => 'Already exists',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        return response()->json($product, 200);
    }

    public function listFavorites($limit = 0)
    {
        $pivot = 0;
        $favorites = DB::table('favorites')->where('user_id', auth()->user()->id)->get()->shuffle();

        $products = array();

        foreach ($favorites as $favorite) {
            $product = Product::where('id', $favorite->product_id)->where('availability', true)->where('archived', false)->get()->first();
            if (!is_null($product) && $product->store->is_active == true) {
                array_push($products, $product);
                $pivot++;
            }

            if ($limit > 0 && $limit == $pivot) break;
        }

        return response()->json($products, 200);
    }

    public function listSimilars($id, $limit = 0)
    {
        $product = Product::find($id);

        if (is_null($product)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        // $products = Array();

        $categories = DB::table('product_category')->where('product_id', $id)->get();

        $products_ids = DB::table('product_category')->whereIn('category_id', $categories->pluck('category_id'))->get()->pluck('product_id')->unique();

        if ($limit > 0) {
            $products = Product::whereIn('id', $products_ids)->where('availability', true)->where('archived', false)->take($limit)->get();
        } else {
            $products = Product::whereIn('id', $products_ids)->where('availability', true)->where('archived', false)->get();
        }

        $arr_products = Array();

        foreach($products as $key => $product){
            if(!$product->store->is_active){
                $products->forget($key);
            } else {
                array_push($arr_products, $product);
            }
        }

        return response()->json($arr_products, 200);
    }

    public function listPublicStarred($limit = 0)
    {
        return $this->listStarred(null, $limit);
    }

    public function listStarred($store, $limit = 0)
    {
        $pivot = 0;

        if ($store) {
            $store = Store::where('store_subdomain', $store)->get()->first();
            $starreds = Product::where('availability', true)->where('archived', false)
                ->where('starred', true)
                ->where('store_id', $store->id)->get()->shuffle();
        } else {
            $starreds = Product::where('starred', true)->where('availability', true)->where('archived', false)->get()->shuffle();
        }

        $products = array();

        foreach ($starreds as $starred) {

            if ($starred->store->is_active == true) {
                unset($starred->questions_and_answers);
                unset($starred->categories);

                if ($starred->store->is_active == true){
                    array_push($products, $starred);
                    $pivot++;
                }

                if ($limit > 0 && $limit == $pivot) break;
            }
        }

        // dd($products);

        return response()->json($products, 200);
    }

    public function listStoreStarred($store, $limit = 0)
    {
        return $this->listStarred($store, $limit);
    }

    public function listPublicDefaultCategory($limit = 0)
    {
        return $this->listDefaultCategory(null, $limit);
    }

    public function listDefaultCategory($store, $limit = 0)
    {
        $pivot = 0;

        $defaultCategory = Category::where('default', true)->get()->first();

        $arr_products = array();

        if (!is_null($defaultCategory)) {
            $arr_products = DB::table('product_category')->where('category_id', $defaultCategory->id)->get()->pluck('product_id')->unique();
        }

        if ($store) {
            $store = Store::where('store_subdomain', $store)->get()->first();
            $chosen = Product::where('availability', true)->where('archived', false)
                ->whereIn('id', $arr_products)
                ->where('store_id', $store->id)->get()->shuffle();
        } else {
            $chosen = Product::whereIn('id', $arr_products)->where('availability', true)->where('archived', false)->get()->shuffle();
        }

        $products = array();

        foreach ($chosen as $product) {
            $product->defaultCategoryName = $defaultCategory->category_name;

            if ($product->store->is_active == true){
                array_push($products, $product);
                $pivot++;
            }
            
            if ($limit > 0 && $limit == $pivot) break;
        }

        return response()->json($products, 200);
    }

    public function toggleArchive(Request $request)
    {
        $request->validate([
            'store_id' => 'required|numeric',
            'id' => 'required|numeric',
        ]);

        // Validate authorized hacked access
        $store = Store::where('id', $request['store_id'])->where('user_id', auth()->user()->id)->first();

        if (is_null($store)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $product = Product::find($request['id']);
        if ($product->archived) {
            $product->archived = false;
        } else {
            $product->archived = true;
        }
        $product->save();

        return response()->json($product, 200);
    }
}
