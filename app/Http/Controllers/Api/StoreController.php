<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\StoreSetting;
use App\Models\Order;
use App\Models\Slide;

class StoreController extends Controller
{

    public function find($store){
        $store = Store::where('store_subdomain', $store)->get()->first();
        return response()->json($store, 200);

    }

    public function list(){
        $stores = Store::where('is_active', true)->get()->shuffle();

        $arr_stores = Array();

        foreach($stores as $store){
            unset($store->products);
            unset($store->orders);
            unset($store->categories);
            unset($store->qualifications);
            array_push($arr_stores, $store);
        }

        // dd($arr_stores);

        return response()->json($arr_stores, 200);
    }    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'store_name' => 'required',
            'store_subdomain' => 'required|unique:stores,store_subdomain,' . $request->get('id'),
            'store_description' => 'required',
            'store_address' => 'required',
            'store_email' => 'required|unique:stores,store_email,' .$request->get('id'),
            'store_phone' => 'required'
        ]);

        $store = Store::find($request->get('id'));

        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $store->fill($request->all());

        $store->save();

        // Validate if the store logo image is received
        if($request->get('image_data')){
            
            $folderPath = "images/" . $store->id . "/";
            $image_parts = explode(";base64,", $request->get('image_data'));
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . uniqid() . '.' . $image_type;

            if(Storage::disk('public_uploads')->put($file, $image_base64)){
                $store_setting = StoreSetting::where('store_id', $store->id)->where('setting_name', 'store_logo')->get()->first();

                if(is_null($store_setting)){
                    $store_setting = [];

                    $store_setting['store_id'] = $store->id;
                    $store_setting['setting_name'] = 'store_logo';
                    $store_setting['setting_data'] = $file;

                    StoreSetting::create($store_setting);
                } else {
                    $store_setting->setting_data = $file;
                    $store_setting->save();
                }
            } else {
                $response = [
                    'error' => 'File upload failed',
                    'message' => 'We could not upload your file'
                ];
                return response()->json($response, 401);
            }
        }     

        return response()->json($store, 200);
    }

    public function getStoreOrders($store){
        // Validate unauthorized hacked access
        $store = Store::where('id', $store)->where('user_id', auth()->user()->id)->first();
                
        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        return Order::where('store_id', $store->id)->orderBy('created_at', 'DESC')->get();
    }

    public function listSlides() {
        $slides = Slide::where('is_active', true)->get();
        return response()->json($slides, 200);
    }
}
