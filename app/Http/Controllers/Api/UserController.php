<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Institute;
use App\Models\User;
use App\Models\Store;
use Illuminate\Support\Facades\Hash;
use ImageOptimizer;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Exception;
use Carbon\Carbon;

class UserController extends Controller
{

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,' . auth()->user()->id,
        ]);

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $user->fill($request->except(['password', 'graduate', 'postgraduate', 'certified', 'courses', 'offices']));

        if ($request->get('password')) {
            $user->password = Hash::make($request->get('password'));
        }

        $user->save();

        // Validate if the profile image is received
        $profile_images = $request->input('image_data.*');


        if (is_array($profile_images) && count($profile_images) > 0) {

            $folderPath = "images/" . $user->id . "/";
            $image_parts = explode(";base64,", $profile_images[0]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . uniqid() . '.' . $image_type;

            if (Storage::disk('public_uploads')->put($file, $image_base64)) {

                // Resize if image is too wide
                $img = Image::make(public_path() . '/uploads/' . $file);

                if ($img->width() > 1200) {
                    $img->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $img->save(public_path() . '/uploads/' . $file);
                }

                // Optimize image
                ImageOptimizer::optimize(public_path() . '/uploads/' . $file);

                $user->profile_image = $file;
                $user->save();
            } else {
                $response = [
                    'error' => 'File upload failed',
                    'message' => 'We could not upload your file'
                ];
                return response()->json($response, 401);
            }
        }

        //Update store's player_id based on email
        $store = Store::where('store_email', $user->email)->get()->first();

        if (!is_null($store)) {
            $store->player_id = $user->player_id;
            $store->save();
        }

        //Updating educational information

        //Graduate studies
        $graduate = json_decode($request->get('graduate'));

        $pregraduate = DB::table('users_institutes')
            ->where('degree_name', 'Graduate')
            ->where('user_id', auth()->user()->id)->get()->first();

        if (property_exists($graduate, 'degree_local_number')){
            if (is_null($pregraduate)) {
                DB::table('users_institutes')->insert(['created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'degree_local_number' => $graduate->degree_local_number, 'degree_national_number' => $graduate->degree_national_number, 'user_id' => auth()->user()->id, 'institute_id' => $graduate->institute_id, 'degree_name' => $graduate->degree_name]);
            } else {
                DB::table('users_institutes')
                    ->where('degree_name', 'Graduate')
                    ->where('user_id', auth()->user()->id)
                    ->update(['updated_at' => Carbon::now(), 'degree_local_number' => $graduate->degree_local_number, 'degree_national_number' => $graduate->degree_national_number, 'institute_id' => $graduate->institute_id]);
            }
        }

        //Postraduate studies
        $postgraduates = json_decode($request->get('postgraduate'));

        foreach ($postgraduates as $postgraduate) {

            //Detect Institute

            $institute = Institute::whereRaw("lower(trim(institute_name)) = '" . strtolower(trim($postgraduate->degree_custom_institute)) . "'")->get()->first();

            if (is_null($institute)) {
                $institute = new Institute();

                $institute->institute_type = 'Universidad';
                $institute->institute_name = $postgraduate->degree_custom_institute;
                $institute->is_active = true;
                $institute->created_at = Carbon::now();
                $institute->updated_at = Carbon::now();

                $institute->save();
            }

            $postgraduate->institute_id = $institute->id;


            if (isset($postgraduate->id)) {
                DB::table('users_institutes')
                    ->where('id', $postgraduate->id)
                    ->update([
                        'updated_at' => Carbon::now(),
                        'degree_name' => 'Postgraduate',
                        'degree_title' => $postgraduate->degree_title,
                        'user_id' => auth()->user()->id,
                        'degree_custom_institute' => $postgraduate->degree_custom_institute,
                        'institute_id' => $postgraduate->institute_id
                    ]);
            } else {
                DB::table('users_institutes')
                    ->insert([
                        'updated_at' => Carbon::now(),
                        'degree_name' => 'Postgraduate',
                        'degree_title' => $postgraduate->degree_title,
                        'user_id' => auth()->user()->id,
                        'degree_custom_institute' => $postgraduate->degree_custom_institute,
                        'institute_id' => $postgraduate->institute_id
                    ]);
            }
        }

        //Certified studies
        $certified = json_decode($request->get('certified'));

        foreach ($certified as $certificate) {

            //Detect Institute

            $institute = Institute::whereRaw("lower(trim(institute_name)) = '" . strtolower(trim($certificate->degree_custom_institute)) . "'")->get()->first();

            if (is_null($institute)) {
                $institute = new Institute();

                $institute->institute_type = 'Instituto';
                $institute->institute_name = $certificate->degree_custom_institute;
                $institute->is_active = true;
                $institute->created_at = Carbon::now();
                $institute->updated_at = Carbon::now();

                $institute->save();
            }

            $certificate->institute_id = $institute->id;

            if (isset($certificate->id)) {
                DB::table('users_institutes')
                    ->where('id', $certificate->id)
                    ->update([
                        'updated_at' => Carbon::now(),
                        'degree_name' => 'Certificate',
                        'degree_title' => $certificate->degree_title,
                        'user_id' => auth()->user()->id,
                        'degree_custom_institute' => $certificate->degree_custom_institute,
                        'institute_id' => $certificate->institute_id
                    ]);
            } else {
                DB::table('users_institutes')
                    ->insert([
                        'updated_at' => Carbon::now(),
                        'degree_name' => 'Certificate',
                        'degree_title' => $certificate->degree_title,
                        'user_id' => auth()->user()->id,
                        'degree_custom_institute' => $certificate->degree_custom_institute,
                        'institute_id' => $certificate->institute_id
                    ]);
            }
        }

        //Courses studies
        $courses = json_decode($request->get('courses'));

        foreach ($courses as $course) {

            //Detect Institute

            $institute = Institute::whereRaw("lower(trim(institute_name)) = '" . strtolower(trim($course->degree_custom_institute)) . "'")->get()->first();

            if (is_null($institute)) {
                $institute = new Institute();

                $institute->institute_type = 'Instituto';
                $institute->institute_name = $course->degree_custom_institute;
                $institute->is_active = true;
                $institute->created_at = Carbon::now();
                $institute->updated_at = Carbon::now();

                $institute->save();
            }

            $course->institute_id = $institute->id;

            if (isset($course->id)) {
                DB::table('users_institutes')
                    ->where('id', $course->id)
                    ->update([
                        'updated_at' => Carbon::now(),
                        'degree_name' => 'Course',
                        'degree_title' => $course->degree_title,
                        'user_id' => auth()->user()->id,
                        'degree_custom_institute' => $course->degree_custom_institute,
                        'institute_id' => $course->institute_id
                    ]);
            } else {
                DB::table('users_institutes')
                    ->insert([
                        'updated_at' => Carbon::now(),
                        'degree_name' => 'Course',
                        'degree_title' => $course->degree_title,
                        'user_id' => auth()->user()->id,
                        'degree_custom_institute' => $course->degree_custom_institute,
                        'institute_id' => $course->institute_id
                    ]);
            }
        }

        //Offices
        $offices = json_decode($request->get('offices'));

        foreach ($offices as $office) {
            if (isset($office->id)) {
                DB::table('offices')
                    ->where('id', $office->id)
                    ->update([
                        'updated_at' => Carbon::now(),
                        'office_name' => $office->office_name,
                        'office_address' => $office->office_address,
                        'office_state' => $office->office_state,
                        'office_location' => $office->office_location,
                        'office_schedule' => json_encode($office->office_schedule),
                        'user_id' => auth()->user()->id
                    ]);
            } else {
                DB::table('offices')
                    ->insert([
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                        'office_name' => $office->office_name,
                        'office_address' => $office->office_address,
                        'office_state' => $office->office_state,
                        'office_location' => $office->office_location,
                        'office_schedule' => json_encode($office->office_schedule),
                        'user_id' => auth()->user()->id
                    ]);
            }
        }

        // Validate if title images are received
        $title_images_ids = $request->input('title_images_ids.*');
        $title_images_data = $request->input('title_images_data.*');


        if (is_array($title_images_ids) && count($title_images_ids) > 0) {

            $folderPath = "images/" . $user->id . "/";

            foreach ($title_images_ids as $key => $value) {
                $image_parts = explode(";base64,", $title_images_data[$key]);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = $folderPath . uniqid() . '.' . $image_type;

                if (Storage::disk('public_uploads')->put($file, $image_base64)) {

                    // Resize if image is too wide
                    $img = Image::make(public_path() . '/uploads/' . $file);

                    if ($img->width() > 1200) {
                        $img->resize(1200, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                        $img->save(public_path() . '/uploads/' . $file);
                    }

                    // Optimize image
                    ImageOptimizer::optimize(public_path() . '/uploads/' . $file);

                    DB::table('users_institutes')->where('id', $value)->update(['degree_picture' => $file]);
                } else {
                    $response = [
                        'error' => 'File upload failed',
                        'message' => 'We could not upload your file'
                    ];
                    return response()->json($response, 401);
                }
            }
        }


        return response()->json($user, 200);
    }

    public function hideOffice(Request $request){
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $office_data = $request->all();

        $office = DB::table('offices')->where('id', $office_data['id'])->update(['is_active' => false]);
        
        if (is_null($office)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        return response()->json($office, 200);
    }

    public function pushMessage(Request $request)
    {

        $this->validate($request, [
            'message' => 'required',
            'receiver' => 'required'
        ]);

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $heading = array("en" => $user->name, "es" => $user->name);
        $content = array("en" => $request->get('message'), "es" => $request->get('message'));

        $fields = array(
            'app_id' => env('ONESIGNAL_APP_ID'),
            'include_player_ids' => array($request->get('receiver')),
            'contents' => $content,
            'headings' => $heading,
            'url' => $request->get('url')
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}

