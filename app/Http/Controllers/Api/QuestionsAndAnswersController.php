<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Store;
use App\Models\User;
use App\Notifications\NewQuestion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class QuestionsAndAnswersController extends Controller
{
    public function list($store)
    {
    }

    public function question(Request $request)
    {
        $request->validate([
            'message' => 'required|string',
            'product_id' => 'required|integer',
        ]);

        $message = $request->all();

        $product = Product::find($message['product_id']);

        if(is_null($product)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        $store = Store::where('id', $product->store_id)->get()->first();

        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        if($store->user_id == auth()->user()->id && (!isset($message['parent_message']) || is_null($message['parent_message']))){
            $response = [
                'error' => 'Not authorized',
                'message' => 'You can only respond to a question',
                'type'  => 'info'
            ];
            return response()->json($response, 401);
        }

        $message['created_by'] = auth()->user()->id;
        $message['created_at'] = Carbon::now();
        $message['updated_at'] = Carbon::now();

        $id = DB::table('questions_and_answers')->insertGetId($message);

        $store_user = User::find($store->user_id);

        $sender_user = User::find(auth()->user()->id);

        if($store_user && $sender_user && $sender_user->id != $store_user->id){
            $store_user->notify(new NewQuestion($sender_user, $message, $product, $store));
        }

        return response()->json(DB::table('questions_and_answers')->where('id', $id)->first(), 200);
    }

    public function answer(Request $request)
    {
        $request->validate([
            'message' => 'required|string',
            'product_id' => 'required|integer',
            'parent_message' => 'required|integer'
        ]);

        $message = $request->all();

        $product = Product::find($message['product_id']);

        if(is_null($product)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        $store = Store::where('id', $product->store_id)->get()->first();

        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        if($store->user_id != auth()->user()->id && is_null($message['parent_message'])){
            $response = [
                'error' => 'Not authorized',
                'message' => 'You can only respond to a question',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        $message['created_by'] = auth()->user()->id;
        $message['created_at'] = Carbon::now();
        $message['updated_at'] = Carbon::now();

        $id = DB::table('questions_and_answers')->insertGetId($message);

        return response()->json(DB::table('questions_and_answers')->where('id', $id)->first(), 200);
    }
    
    public function questions($store)
    {
        $store = Store::where('store_subdomain', $store)->where('user_id', auth()->user()->id)->get()->first();

        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        $products = Array();

        foreach($store->products as $key => $product){

            $unanswered = Array();
            $answered = Array();
            
            // Obtaining messages
            $messages = DB::table('questions_and_answers')->where('product_id', $product->id)->get();


            foreach($messages as $message){

                // Detect weather is a question
                if(is_null($message->parent_message)){

                    // Detect if was answered
                    $was_answered = DB::table('questions_and_answers')->where('product_id', $product->id)->where('parent_message', $message->id)->first();

                    // If is was not respondend, add it to pending questions for the given product
                    if(is_null($was_answered)){
                        array_push($unanswered, $message);
                    } else {
                        array_push($answered, $message);
                    }
                }
            }

            if(count($answered) == 0 && count($unanswered) == 0){
                $store->products->forget($key);
            } else {
                $product['answered'] = $answered;
                $product['unanswered'] = $unanswered;
                array_push($products, $product);
            }

            
        }

        return response()->json($products, 200);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'product_id' => 'required|integer',
            'parent_message' => 'required|integer'
        ]);

        $message = $request->all();

        $product = Product::find($message['product_id']);

        if(is_null($product)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        $store = Store::where('id', $product->store_id)->get()->first();

        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        if($store->user_id != auth()->user()->id){
            $response = [
                'error' => 'Not authorized',
                'message' => 'You can only delete a question of your store',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }

        $id = DB::table('questions_and_answers')
            ->where('id', $message['parent_message'])
            ->orWhere('parent_message', $message['parent_message'])->delete();

        return response()->json(DB::table('questions_and_answers')->where('id', $id)->first(), 200);
    }
}