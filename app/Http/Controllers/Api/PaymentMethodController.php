<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PaymentMethod;

class PaymentMethodController extends Controller
{
    public function list()
    {
        $payment_methods = PaymentMethod::all();
        return response()->json($payment_methods, 200);
    }
}
