<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Notifications\VerifyUser;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PatientController extends Controller
{
    public function registerPatient(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required',
            'date_of_birth' => 'required',
            'tax_id' => 'required'
        ]);

        $validatedData = $request->all();

        $user = User::where('email', strtolower($validatedData['email']))->get()->first();

        if (!is_null($user)) {
            $user->date_of_birth = $validatedData['date_of_birth'];
            $user->save();
            $user->assignRole('patient');
        } else {
            $user = User::create([
                'name' => $validatedData['name'],
                'email' => strtolower($validatedData['email']),
                'last_name' => $validatedData['last_name'],
                'phone' => $validatedData['phone'],
                'date_of_birth' => $validatedData['date_of_birth'],
                'password' => Hash::make($validatedData['tax_id']),
                'tax_id' => $validatedData['tax_id']
            ]);

            $user->assignRole('patient');

            $token = $user->createToken('auth_token')->plainTextToken;
            $user->notify(new VerifyUser($user));
        }

        return response()->json($user, 200);
    }

    public function updatePatient(Request $request)
    {
        $request->validate([
            'patient_id' => 'required',
            'tax_id_letter' => 'required|string',
            'tax_id' => 'required',
            'date_of_birth' => 'required',
        ]);

        $validatedData = $request->all();

        $user = User::where('id', $validatedData['patient_id'])->get()->first();

        if (!is_null($user)) {
            $user->date_of_birth = substr($validatedData['date_of_birth'], 0, 10);
            $user->tax_id_letter = $validatedData['tax_id_letter'];
            $user->tax_id = $validatedData['tax_id'];
            $user->save();
        } else {
            $response = [
                'error' => 'Not found',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 404);
        }

        return response()->json($user, 200);
    }

    public function patientSearch($query)
    {
        $patients = User::whereRaw("UPPER(email) LIKE '%" . strtoupper($query) . "%'")
            ->orWhereRaw("UPPER(name) LIKE '%" . strtoupper($query) . "%'")
            ->orWhereRaw("UPPER(last_name) LIKE '%" . strtoupper($query) . "%'")
            ->orWhereRaw("UPPER(tax_id) LIKE '%" . strtoupper($query) . "%'")->get();
        
        return response()->json($patients, 200);
    }

    public function myPatients()
    {
        $patients = DB::table('dentist_patient')->where('dentist_id', auth()->user()->id)->get();
        $arr_patients = Array();

        foreach($patients as $patient){
            $personal_history = $patient->personal_history;
            $patient = User::find($patient->patient_id);
            if(!is_null($patient)){
                $patient->treatments = DB::table('patient_treatment')->where('patient_id', $patient->id)->where('is_active', true)->get();
                $patient->appointments = Appointment::whereRaw('dentist_id = ? and (patient_id = ? or patient_email = ?)', [auth()->user()->id, $patient->id, $patient->email])->get();
                $patient->family_history = DB::table('relatives_medical_history')->where('patient_id', $patient->id)->get();
                $patient->personal_history = $personal_history;
                array_push($arr_patients, $patient);
            }
        }

        return response()->json($arr_patients, 200);
    }

    public function addTreatment(Request $request){
        $request->validate([
            'patient_id' => 'required',
            'name' => 'required',
            'notes' => 'required',
            'treatment_date' => 'required'
        ]);

        $validatedData = $request->all();

        $validatedData['dentist_id'] = auth()->user()->id;

        $validatedData['treatment_date'] = Carbon::createFromFormat('Y-m-d H:i:s', str_replace('T', ' ', substr($validatedData['treatment_date'], 0, 19)));
        $validatedData['created_at'] = Carbon::now();
        $validatedData['updated_at'] = Carbon::now();

        if(isset($validatedData['office_id']) && !is_null($validatedData['office_id'])){
            $validatedData['location'] = DB::table('offices')->where('id', $validatedData['office_id'])->get()->first()->office_name;
        }

        $inserted = DB::table('patient_treatment')->insert($validatedData);

        return response()->json($validatedData, 200);
    }

    public function hideTreatment(Request $request){
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $treatment_data = $request->all();

        $treatment = DB::table('patient_treatment')->where('id', $treatment_data['id'])->update(['is_active' => false]);
        
        if (is_null($treatment)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        return response()->json($treatment, 200);
    }

    public function addFamilyHistory(Request $request){
        $request->validate([
            'patient_id' => 'required',
            'family_history_parenthood' => 'required',
            'family_history_medical' => 'required',
        ]);

        $validatedData = $request->all();

        $validatedData['dentist_id'] = auth()->user()->id;

        $validatedData['created_at'] = Carbon::now();
        $validatedData['updated_at'] = Carbon::now();

        $inserted = DB::table('relatives_medical_history')->insertGetId($validatedData);

        $validatedData['id'] = $inserted;

        return response()->json($validatedData, 200);
    }

    public function removeFamilyHistory(Request $request){
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $family_history_data = $request->all();

        $family_history = DB::table('relatives_medical_history')->delete($family_history_data['id']);
        
        if (is_null($family_history)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        return response()->json($family_history, 200);
    }

    public function updatePersonalHistory(Request $request){
        $request->validate([
            'patient_id' => 'required|numeric'
        ]);

        $personal_history_data = $request->all();

        $personal_history = DB::table('dentist_patient')->where('patient_id', $personal_history_data['patient_id'])->where('dentist_id', auth()->user()->id)->update(['personal_history' => $personal_history_data['personal_history']]);
        
        if (is_null($personal_history)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        return response()->json($personal_history, 200);
    }
}
