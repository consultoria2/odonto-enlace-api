<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Institute;


class InstitutesController extends Controller
{
    public function list($institute_type = null)
    {
        $institutes = Array();
        if($institute_type){
            $institutes = Institute::where('is_active', true)->where('institute_type', $institute_type)->get();
        } else {
            $institutes = Institute::where('is_active', true)->get();
        }
        return response()->json($institutes, 200);
    }
}
