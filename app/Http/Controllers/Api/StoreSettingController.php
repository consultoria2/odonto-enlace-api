<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Store;
use App\Models\StoreSetting;

use Illuminate\Support\Facades\DB;

class StoreSettingController extends Controller
{
    public function paymentOptions($store)
    {
        $store_id = Store::where('store_subdomain', $store)->first()->id;
        $payment_options = StoreSetting::where('store_id', $store_id)->where('setting_name', 'payment_option')->get();

        foreach($payment_options as $option){
            $option->setting_data = json_decode($option->setting_data);
        }

        return $payment_options;
    }

    public function storePaymentOption(Request $request)
    {
        $request->validate([
            'store_subdomain' => 'required|string',
            'name' => 'required|string',
            'currency' => 'required|string',
            'text' => 'required|string',
        ]);

        $payment_option = $request->all();

        $store_setting = Array();
        $store_setting['store_id'] = Store::where('store_subdomain', $request->store_subdomain)->first()->id;
        $store_setting['setting_name'] = 'payment_option';
        $store_setting['setting_data'] = json_encode($payment_option);

        return StoreSetting::create($store_setting);
    }

    public function deletePaymentOption(Request $request)
    {
        $request->validate([
            'store_subdomain' => 'required|string',
            'id' => 'required|numeric',
        ]);

        $store_id = Store::where('store_subdomain', $request->store_subdomain)->first()->id;
        return StoreSetting::where('store_id', $store_id)->where('id', $request->id)->delete();
    }

    public function setStoreInformation(Request $request){
        $request->validate([
            'store_subdomain' => 'required|string'
        ]);

        // Validate authorized hacked access
        $store = Store::where('store_subdomain', $request['store_subdomain'])->where('user_id', auth()->user()->id)->first();
                
        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }        

        $store_information = $request->all();

        $store_setting = Array();
        $store_setting['store_id'] = $store->id;
        $store_setting['setting_name'] = 'store_information';
        $store_setting['setting_data'] = json_encode($store_information);

        $setting = StoreSetting::updateOrCreate(
            ['store_id' => $store_setting['store_id'], 'setting_name' => $store_setting['setting_name']],
            ['setting_data' => $store_setting['setting_data']]
        );

        $store->dirty = true;
        $store->save();

        $response = [
            'success' => 'Action performed',
            'message' => 'Row count: ' . $setting
        ];

        return response()->json($response, 200);
    }

    public function getStoreInformation($store)
    {
        $store_id = Store::where('store_subdomain', $store)->first()->id;
        $store_information = StoreSetting::where('store_id', $store_id)->where('setting_name', 'store_information')->get()->first();

        if(!is_null($store_information)){
            return $store_information->setting_data;
        } else {
            return null;
        }
    }

    public function setStoreGallery(Request $request){
        $request->validate([
            'store_subdomain' => 'required|string'
        ]);

        // Validate authorized hacked access
        $store = Store::where('store_subdomain', $request['store_subdomain'])->where('user_id', auth()->user()->id)->first();
                
        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }        

        $store_information = $request->all();

        $store_setting = Array();
        $store_setting['store_id'] = $store->id;
        $store_setting['setting_name'] = 'store_gallery';
        

        $image_file = $request->file('uploading_image_file');

        if($request->hasFile('uploading_image_file')) {
            $store_gallery = json_decode($store_information['store_gallery']);
            $store_gallery[$request['uploading_index']]->image_file = $image_file->store('stores/' . $request->store_subdomain . '/gallery');
            $store_information['store_gallery'] = json_encode($store_gallery);
        }

        $store_setting['setting_data'] = json_encode($store_information);

        $setting = StoreSetting::updateOrCreate(
            ['store_id' => $store_setting['store_id'], 'setting_name' => $store_setting['setting_name']],
            ['setting_data' => $store_setting['setting_data']]
        );

        $response = [
            'success' => 'Action performed',
            'message' => 'Row count: ' . $setting
        ];

        return response()->json($response, 200);
    }

    public function getStoreImages($store)
    {
        $store = Store::where('store_subdomain', $store)->first();
                
        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $store_id = Store::where('store_subdomain', $store->store_subdomain)->first()->id;

        $store_information = DB::table('store_settings')->where('store_id', $store_id)->whereRaw("setting_name like 'store_image_%'")->orderBy('setting_name', 'ASC')->get();

        // Search for URLs and give the appropiate format
        foreach($store_information as $item){
            if(isset($item->setting_data)){
                if(is_string($item->setting_data) && stripos($item->setting_data, 'http') === FALSE){
                    $item->setting_data = env('APP_URL') . '/uploads/' . $item->setting_data;
                }
            }
        }

        if(!is_null($store_information)){
            return response()->json($store_information, 200);
        } else {
            return response()->json($store_information, 404);
        }
    }

    public function getStoreCategories($store)
    {
        $store = Store::where('store_subdomain', $store)->first();
                
        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $store_id = Store::where('store_subdomain', $store->store_subdomain)->first()->id;

        $store_information = DB::table('store_settings')->where('store_id', $store_id)->whereRaw("setting_name like 'store_category_%'")->orderBy('setting_name', 'ASC')->get();


        if(!is_null($store_information)){
            return response()->json($store_information, 200);
        } else {
            return response()->json($store_information, 404);
        }
    }

    public function setStoreImage(Request $request){
        $request->validate([
            'store_id' => 'required|numeric',
            'store_image_index' => 'required|numeric'
        ]);

        $store_data = $request->all();

        // Validate unauthorized hacked access
        $store = Store::where('id', $request['store_id'])->where('user_id', auth()->user()->id)->first();
                
        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $store_image = $request->input('store_image');

        $folderPath = "images/" . $store->id . "/";
        $image_parts = explode(";base64,", $store_image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $name = $folderPath . uniqid() . '.' . $image_type;

        $setting_name = 'store_image_' . $store_data['store_image_index'];

        // Remove previous image if exists
        $response = DB::table('store_settings')->where(['store_id' => $store_data['store_id'], 'setting_name' => $setting_name])->first();

        // dd($response);

        if(!is_null($response)){
            Storage::disk('public_uploads')->delete($response->setting_data);
            DB::table('store_settings')->where(['store_id' => $store_data['store_id'], 'setting_name' => $setting_name])->delete();
        }
    
        if(Storage::disk('public_uploads')->put($name, $image_base64)){

            $store_setting = DB::table('store_settings')->insert(['store_id' => $store_data['store_id'], 'setting_name' => $setting_name, 'setting_data' => $name]);

        } else {
            $response = [
                'error' => 'File upload failed',
                'message' => 'We could not upload your file'
            ];
            return response()->json($response, 401);
        }

        return response()->json($store_setting, 200);
    }

    public function setStoreCategory(Request $request){
        $request->validate([
            'store_id' => 'required|numeric',
            'store_category_index' => 'required|numeric',
            'store_category_value' => 'required|numeric'
        ]);

        $store_data = $request->all();

        // Validate unauthorized hacked access
        $store = Store::where('id', $request['store_id'])->where('user_id', auth()->user()->id)->first();
                
        if(is_null($store)){
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }


        $setting_name = 'store_category_' . $store_data['store_category_index'];

        // Remove previous category setting if exists
        $response = DB::table('store_settings')->where(['store_id' => $store_data['store_id'], 'setting_name' => $setting_name])->delete();

        $store_setting = DB::table('store_settings')->insert(['store_id' => $store_data['store_id'], 'setting_name' => $setting_name, 'setting_data' => $store_data['store_category_value']]);

        return response()->json($store_setting, 200);
    }
}
