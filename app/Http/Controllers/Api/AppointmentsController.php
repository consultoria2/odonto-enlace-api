<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Notifications\VerifyUser;
use App\Notifications\AppointmentCreated;
use App\Notifications\AppointmentRemoved;

class AppointmentsController extends Controller
{
    public function myAppointments($today = false)
    {
        if (!$today){
            $appointments = Appointment::where('dentist_id', auth()->user()->id)->get();
        } else {
            $appointments = Appointment::where('dentist_id', auth()->user()->id)->whereRaw("date(appointment_date) = date(now())")->get();
        }
        
        return response()->json($appointments, 200);
    }

    public function todayAppointments(){
        $today = true;
        return $this->myAppointments($today);
    }

    public function createAppointment(Request $request){
        $request->validate([
            'notes' => 'required|string',
            'patient_name' => 'required|string',
            'patient_phone' => 'required|string',
            'patient_email' => 'string|email',
        ]);

        $appointment_data = $request->all();
        
        $appointment_data['appointment_date'] = Carbon::createFromFormat('Y-m-d H:i:s', str_replace('T', ' ', substr($appointment_data['appointment_date'], 0, 19)));
        $appointment_data['dentist_id'] = auth()->user()->id;
        $appointment_data['created_at'] = Carbon::now();
        $appointment_data['updated_at'] = Carbon::now();

        $appointment = Appointment::create($appointment_data);

        $missing_patient = true;

        if(isset($appointment_data['patient_id'])){
            $patient = User::find($appointment_data['patient_id']);
            if (!is_null($patient)){
                DB::table('dentist_patient')->upsert(['updated_at' => Carbon::now()], ['dentist_id' => auth()->user()->id, 'patient_id' => $appointment_data['patient_id']]);
                $missing_patient = false;
            }
        } else if (isset($appointment_data['patient_email'])){
            $patient = User::where('email', strtolower($appointment_data['patient_email']))->get()->first();
            if (!is_null($patient)){
                DB::table('dentist_patient')->upsert([
                    ['dentist_id' => auth()->user()->id, 'patient_id' => $patient->id, 'updated_at' => Carbon::now()]
                ], 
                ['dentist_id', 'patient_id'], 
                ['updated_at']);
                $missing_patient = false;
            }
        } else if (isset($appointment_data['patient_phone'])){
            $patient = User::where('phone', str_replace('+58', '0', $appointment_data['patient_phone']))->get()->first();
            if (!is_null($patient)){
                DB::table('dentist_patient')->upsert([
                    ['dentist_id' => auth()->user()->id, 'patient_id' => $patient->id, 'updated_at' => Carbon::now()]
                ], 
                ['dentist_id', 'patient_id'], 
                ['updated_at']);
                $missing_patient = false;
            }
        }

        // Notify appointment

        $appointment->notify(new AppointmentCreated($appointment));

        // If patient's user does not exist

        if($missing_patient){
            $random = Str::random(10);

            $user = User::create([
                'name' => $appointment_data['patient_name'],
                'email' => isset($appointment_data['patient_email']) ? strtolower($appointment_data['patient_email']) : $appointment_data['patient_phone'],
                'phone' => $appointment_data['patient_phone'],
                'password' => Hash::make($random),
                'status' => 'invited'
            ]);

            $user->assignRole('patient');

            $token = $user->createToken('auth_token')->plainTextToken;

            if ($user->email && filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                $user->notify(new VerifyUser($user, $random));
            }

            DB::table('dentist_patient')->upsert([['dentist_id' => auth()->user()->id, 'patient_id' => $user->id, 'updated_at' => Carbon::now()]], ['dentist_id', 'patient_id'], ['updated_at']);
        }

        return response()->json($appointment, 200);
    }

    public function removeAppointment(Request $request){
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $appointment_data = $request->all();

        $appointment = Appointment::where('id', $appointment_data['id'])->where('dentist_id', auth()->user()->id)->get()->first();
        
        if (is_null($appointment)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        // Notify remove appointment
        $appointment->notify(new AppointmentRemoved($appointment));

        $response = $appointment->delete();

        return response()->json($response, 200);
    }
}
