<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExchangeRate;
use DB;
use Exception;
use Faker\Provider\ar_SA\Payment;
use Illuminate\Support\Facades\Artisan;

class ExchangeRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $exchange_rates = ExchangeRate::where('to_currency', 'VES')->orderBy('fixed', 'DESC')->orderBy('id', 'DESC')->paginate(5);
        return view('exchange-rates.index', compact('exchange_rates'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('payment-methods.create');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exchange_rate = ExchangeRate::find($id);
        return view('exchange-rates.edit', compact('exchange_rate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'rate' => 'required|numeric'
        ]);


        $element = ExchangeRate::find($id);

        $element->rate = $request->get('rate');
        $element->edited = true;
        $element->save();


        return redirect()->route('exchange-rates.index')
            ->with('success', 'Tasa actualizada satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id', $id)->delete();
        return redirect()->route('roles.index')
            ->with('success', 'Rol eliminado satisfactoriamente');
    }



    public function fixate($id){
        $selectedRate = ExchangeRate::find($id);

        $rates = ExchangeRate::where('from_currency', $selectedRate->from_currency)
                            ->where('to_currency', $selectedRate->to_currency)
                            ->where('fixed', true)
                            ->get();

        foreach($rates as $rate){
            $rate->fixed = false;
            $rate->save();
        }

        $selectedRate->fixed = true;
        $selectedRate->save();

        return redirect()->route('exchange-rates.index')->with('success','Tasa fijada con exito. Se empleará esta tasa como la actual');
    }

    public function release($id){
        $selectedRate = ExchangeRate::find($id);

        $selectedRate->fixed = false;
        $selectedRate->save();

        return redirect()->route('exchange-rates.index')->with('success','Tasa liberada con exito. Se empleará la última cotización como tasa válida');
    }
    
    public function refresh() {
        try {
            Artisan::call('rates:update');
        } catch (Exception $e) {
            return redirect()->route('exchange-rates.index')
                ->with('error', $e);
        }

        return redirect()->route('exchange-rates.index')
            ->with('success', 'Tasa obtenida satisfactoriamente');
    }
}
