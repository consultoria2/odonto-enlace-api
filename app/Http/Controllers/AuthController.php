<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Notifications\VerifyUser;
use App\Notifications\VerifiedUser;
use App\Notifications\NewPasswordCreated;
use Illuminate\Support\Facades\Log;
use Exception;

class AuthController extends Controller
{
  public function register(Request $request)
  {
    $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:8',
      'last_name' => 'required|string|max:255',
      // 'location' => 'required|string|max:255',
      'state' => 'required|string|max:255',
      //TODO: Validate thar roles exists
      'roles' => 'required'
    ]);

    $user = User::create([
      'name' => $validatedData['name'],
      'email' => $validatedData['email'],
      'password' => Hash::make($validatedData['password']),
      'last_name' => $validatedData['last_name'],
      'state' => $validatedData['state'],
      'verification_token' => Str::random(20)
    ]);

    $token = $user->createToken('auth_token')->plainTextToken;

    $user->assignRole($validatedData['roles']);

    try {
      $user->notify(new VerifyUser($user));
    } catch (Exception $e) {

    } 

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ], 200);
  }

  public function login(Request $request)
  {
    if (!Auth::attempt($request->only('email', 'password'))) {
      return response()->json([
        'message' => 'Invalid login details',
        'type' => 'info'
      ], 401);
    }

    $user = User::where('email', $request['email'])->firstOrFail();

    if(in_array($user->status, Array('created', 'invited'))){
      if (is_null($user->verification_token) || strlen($user->verification_token) < 10) {
        $user->verification_token = Str::random(20);
        $user->save();
      }

      try {
        $user->notify(new VerifyUser($user));
      } catch (Exception $e) {

      }
      
      return response()->json([
        'message' => 'Please check your mail and verify your email',
        'type' => 'info'
      ], 401);
    }

    $token = $user->createToken('auth_token')->plainTextToken;

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ], 200);
  }

  public function setPlayerId(Request $request)
  {
    $user = User::find(auth()->user()->id);

    if (!is_null($user)) {
      $user->player_id = $request->get('player_id');
      $user->save();
      return response()->json($user, 200);
    } else {
      return response()->json([
        'message' => 'User not found',
        'type' => 'info'
      ], 401);
    }
  }

  public function me(Request $request)
  {
    return $request->user();
  }

  public function forgotPassword(Request $request)
  {
    $validatedData = $request->validate([
      'email' => 'required|string|email|max:255|exists:users',
    ]);

    $random = Str::random(10);

    $user = User::where('email', $validatedData['email'])->whereNotIn('email', ['romeroaaron33@gmail.com'])->get()->first();

    if (is_null($user)) {
      return response()->json([
        'message' => 'User not found',
        'type' => 'info'
      ], 401);
    }

    $user->password = Hash::make($random);
    $user->save();

    try {
      $user->notify(new NewPasswordCreated($user, $random));
    } catch (Exception $e){
      Log::error('Error sending email to User: ' . $user);
      Log::error($e);
    }

    return response()->json($user, 200);
  }

  public function setPassword(Request $request)
  {
    $validatedData = $request->validate([
      'password' => 'required|string',
    ]);

    $user = User::find(auth()->user()->id);

    if (is_null($user)) {
      return response()->json([
        'message' => 'User not found',
        'type' => 'info'
      ], 401);
    }

    $user->password = Hash::make($validatedData['password']);
    $user->save();

    try {
      $user->notify(new NewPasswordCreated($user, $validatedData['password']));
    } catch (Exception $e){
      Log::error('Error sending email to User: ' . $user);
      Log::error($e);
    }

    return response()->json($user, 200);
  }

  public function verify($verification_token) {
    $user = User::where('verification_token', $verification_token)->whereIn('status', Array('created', 'invited', 'verified'))->get()->first();

    if (is_null($user)) {
      return response()->json([
        'message' => 'User not found',
        'type' => 'info'
      ], 401);
    }

    $user->notify(new VerifiedUser($user));

    if(in_array($user->status, Array('created', 'invited'))){
      $user->status = 'verified';
      $user->save();

      return redirect()->away(env('HOME_URL') . '?code=001');

    } else {
      return redirect()->away(env('HOME_URL') . '?code=002');
    }
  }
}
