<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\User;
use App\Models\Order;
use App\Notifications\NewStoreOrder;
use DB;
use Exception;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $orders = Order::orderBy('id', 'DESC')->paginate(10);
        // return view('orders.index', compact('orders'))
        //     ->with('i', ($request->input('page', 1) - 1) * 10);

        $orders = Order::orderBy('id', 'DESC')->get();
        return view('orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all()->pluck('name', 'id');
        return view('stores.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_name' => 'required',
            'store_subdomain' => 'required|unique:stores,store_subdomain',
            'store_description' => 'required',
            'store_address' => 'required',
            'store_email' => 'required|unique:stores,store_email',
            'store_phone' => 'required',
            'user_id' => 'required',
        ]);

        $store = Store::create($request->all());

        return redirect()->route('stores.index')
            ->with('success', 'Tienda creada satisfactoriamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::find($id);
        $users = User::all()->pluck('name', 'id');
        return view('stores.edit', compact('store', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'store_name' => 'required',
            'store_subdomain' => 'required|unique:stores,store_subdomain,' . $id,
            'store_description' => 'required',
            'store_address' => 'required',
            'store_email' => 'required|unique:stores,store_email,' .$id,
            'store_phone' => 'required',
            'user_id' => 'required',
        ]);

        $store = Store::find($id);
        $store->fill($request->all());
        $store->save();

        return redirect()->route('stores.index')
            ->with('success', 'Tienda actualizada satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id', $id)->delete();
        return redirect()->route('roles.index')
            ->with('success', 'Rol eliminado satisfactoriamente');
    }

    /**
     * Cancel the specified Order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $order = Order::find($id);
        $order->status = 'cancelled';
        $order->save();

        return redirect()->route('orders.index')
            ->with('success', 'Orden cancelada satisfactoriamente');
    }

    /**
     * Approve the specified Order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        $order = Order::find($id);
        $order->status = 'approved';
        $order->save();

        $store = Store::find($order->store_id);

        try {
            $store->notify(new NewStoreOrder($order, $store));
        } catch (Exception $e) {

        }        

        return redirect()->route('orders.index')
            ->with('success', 'Orden aprobada satisfactoriamente');
    }     
    
    /**
     * Approve the specified Order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deliver($id)
    {
        $order = Order::find($id);
        $order->status = 'delivering';
        $order->save();

        return redirect()->route('orders.index')
            ->with('success', 'Orden marcada como siendo despachada satisfactoriamente');
    }    

    /**
     * Approve the specified Order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delivered($id)
    {
        $order = Order::find($id);
        $order->status = 'delivered';
        $order->save();

        return redirect()->route('orders.index')
            ->with('success', 'Orden marcada como entregada satisfactoriamente');
    }    
}
