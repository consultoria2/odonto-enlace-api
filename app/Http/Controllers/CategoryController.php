<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use App\Models\Category;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::orderBy('id', 'DESC')->paginate(10);
        return view('categories.index', compact('categories'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required|unique:categories,category_name'
        ]);

        $category = Category::create(['category_name' => $request->input('category_name')]);

        return redirect()->route('categories.index')
            ->with('success', 'Categoría creada satisfactoriamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_name' => 'required|unique:categories,category_name'
        ]);

        $category = Category::find($id);
        $category->category_name = $request->input('category_name');
        $category->save();

        return redirect()->route('categories.index')
            ->with('success', 'Categoria actualizada satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $products = DB::table('product_category')->where('category_id', $id)->get()->first();

        if (!is_null($products)) {
            return redirect()->route('categories.index')
            ->with('error', 'La categoría tiene productos asociados y no puede ser eliminada');
        }

        DB::table("categories")->where('id', $id)->delete();

        return redirect()->route('categories.index')
            ->with('success', 'Categoría eliminada satisfactoriamente');
    }

    /**
     * Suspend the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function suspend($id)
    {
        $category = Category::find($id);
        $category->is_active = false;
        $category->save();

        return redirect()->route('categories.index')
            ->with('success', 'Categoría suspendida satisfactoriamente');
    }

    /**
     * Activate the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $category = Category::find($id);
        $category->is_active = true;
        $category->save();

        return redirect()->route('categories.index')
            ->with('success', 'Categoría activada satisfactoriamente');
    }
}
