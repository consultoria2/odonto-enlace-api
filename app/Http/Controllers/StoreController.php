<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\User;
use DB;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stores = Store::orderBy('id', 'DESC')->get();
        return view('stores.index', compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all()->pluck('name', 'id');
        return view('stores.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_name' => 'required',
            'store_subdomain' => 'required|unique:stores,store_subdomain',
            'store_description' => 'required',
            'store_address' => 'required',
            'store_email' => 'required|unique:stores,store_email',
            'store_phone' => 'required',
            'user_id' => 'required',
        ]);

        $element = $request->all();

        $element['store_subdomain'] = str_replace(' ', '-', trim(strtolower($element['store_subdomain'])));

        $store = Store::create($request->all());

        return redirect()->route('stores.index')
            ->with('success', 'Tienda creada satisfactoriamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::find($id);
        $users = User::all()->pluck('name', 'id');
        return view('stores.edit', compact('store', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'store_name' => 'required',
            'store_subdomain' => 'required|unique:stores,store_subdomain,' . $id,
            'store_description' => 'required',
            'store_address' => 'required',
            'store_email' => 'required|unique:stores,store_email,' .$id,
            'store_phone' => 'required',
            'user_id' => 'required',
        ]);

        $store = Store::find($id);

        $element = $request->all();

        $element['store_subdomain'] = str_replace(' ', '-', trim(strtolower($element['store_subdomain'])));

        $store->fill($element);
        $store->save();

        return redirect()->route('stores.index')
            ->with('success', 'Tienda actualizada satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id', $id)->delete();
        return redirect()->route('roles.index')
            ->with('success', 'Rol eliminado satisfactoriamente');
    }

    /**
     * Suspend the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function suspend($id)
    {
        $store = Store::find($id);
        $store->is_active = false;
        $store->save();

        return redirect()->route('stores.index')
            ->with('success', 'Tienda suspendida satisfactoriamente');
    }

    /**
     * Activate the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $store = Store::find($id);
        $store->is_active = true;
        $store->save();

        return redirect()->route('stores.index')
            ->with('success', 'Tienda activada satisfactoriamente');
    }

    public function hide($id)
    {
        $store = Store::find($id);
        $store->allow_navigation = false;
        $store->save();

        return redirect()->route('stores.index')
            ->with('success', 'Navegacion suspendida satisfactoriamente');
    }

    public function show($id)
    {
        $store = Store::find($id);
        $store->allow_navigation = true;
        $store->save();

        return redirect()->route('stores.index')
            ->with('success', 'Navegacion permitida satisfactoriamente');
    }
}
