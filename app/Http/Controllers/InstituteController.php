<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use App\Models\Institute;
use DB;

class InstituteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $institutes = Institute::orderBy('id', 'DESC')->paginate(10);
        return view('institutes.index', compact('institutes'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('institutes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'institute_name' => 'required|unique:institutes,institute_name',
            'institute_type' => 'required'
        ]);

        $institute = Institute::create(['institute_name' => $request->input('institute_name'), 'institute_type' => $request->input('institute_type')]);

        return redirect()->route('institutes.index')
            ->with('success', 'Institución creada satisfactoriamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institute = Institute::find($id);
        return view('institutes.edit', compact('institute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'institute_name' => 'required|unique:institutes,institute_name,' . $id,
            'institute_type' => 'required'
        ]);

        $institute = Institute::find($id);
        $institute->institute_name = $request->input('institute_name');
        $institute->institute_type = $request->input('institute_type');
        $institute->save();

        return redirect()->route('institutes.index')
            ->with('success', 'Institución actualizada satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = DB::table('users')->where('institute_id', $id)->get()->first();

        if (!is_null($user)) {
            return redirect()->route('intitutes.index')
            ->with('error', 'La institución tiene usuarios asociados y no puede ser eliminada');
        }

        DB::table("institutes")->where('id', $id)->delete();

        return redirect()->route('institutes.index')
            ->with('success', 'Institución eliminada satisfactoriamente');
    }

    /**
     * Suspend the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function suspend($id)
    {
        $institute = Institute::find($id);
        $institute->is_active = false;
        $institute->save();

        return redirect()->route('institutes.index')
            ->with('success', 'Institución suspendida satisfactoriamente');
    }

    /**
     * Activate the specified Store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $institute = Institute::find($id);
        $institute->is_active = true;
        $institute->save();

        return redirect()->route('institutes.index')
            ->with('success', 'Institución activada satisfactoriamente');
    }
}
