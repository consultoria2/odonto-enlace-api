<?php

use Illuminate\Http\Request;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\StoreController;
use App\Http\Controllers\Api\StoreSettingController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CategoriesController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\PaymentMethodController;
use App\Http\Controllers\Api\QuestionsAndAnswersController;
use App\Http\Controllers\Api\ExchangeRateController;
use App\Http\Controllers\Api\PatientController;
use App\Http\Controllers\Api\AppointmentsController;
use App\Http\Controllers\Api\InstitutesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');
Route::post('/set-player-id', [AuthController::class, 'setPlayerId'])->middleware('auth:sanctum');
Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);
Route::get('/set-password', [AuthController::class, 'setPassword'])->middleware('auth:sanctum');
Route::get('/verify/{verification_token}', [AuthController::class, 'verify']);

//-----------------------------------------------------------------------------------------
//  PRODUCTS PRIVATE ACCESS
//-----------------------------------------------------------------------------------------

Route::post('/products/create', [ProductController::class, 'store'])->middleware('auth:sanctum');
Route::post('/products/edit', [ProductController::class, 'edit'])->middleware('auth:sanctum');
Route::post('/products/remove/picture', [ProductController::class, 'removePicture'])->middleware('auth:sanctum');
Route::post('/products/delete', [ProductController::class, 'delete'])->middleware('auth:sanctum');
Route::post('/products/archive', [ProductController::class, 'toggleArchive'])->middleware('auth:sanctum');
Route::get('/products/{store}/list', [ProductController::class, 'list'])->middleware('auth:sanctum');

Route::get('/products/{store}/list/{limit}', [ProductController::class, 'list'])->middleware('auth:sanctum');
Route::get('/guest/products/{store}/list/{limit}', [ProductController::class, 'list']);


// Route::get('/products/{store}/product/{id}', [ProductController::class, 'view']);
Route::get('/products/offers', [ProductController::class, 'listPublicOffers'])->middleware('auth:sanctum');
Route::get('/products/offers/{limit}', [ProductController::class, 'listPublicOffers'])->middleware('auth:sanctum');

Route::get('guest/products/offers', [ProductController::class, 'listPublicOffers']);
Route::get('guest/products/offers/{limit}', [ProductController::class, 'listPublicOffers']);

Route::get('/products/{store}/offers', [ProductController::class, 'listStoreOffers'])->middleware('auth:sanctum');
Route::get('/guest/products/{store}/offers', [ProductController::class, 'listStoreOffers']);

Route::get('/products/{store}/offers/{limit}', [ProductController::class, 'listStoreOffers'])->middleware('auth:sanctum');
Route::get('/guest/products/{store}/offers/{limit}', [ProductController::class, 'listStoreOffers']);

Route::get('/products/starred', [ProductController::class, 'listPublicStarred'])->middleware('auth:sanctum');
Route::get('/products/starred/{limit}', [ProductController::class, 'listPublicStarred'])->middleware('auth:sanctum');

Route::get('/guest/products/starred', [ProductController::class, 'listPublicStarred']);
Route::get('/guest/products/starred/{limit}', [ProductController::class, 'listPublicStarred']);

Route::get('/products/{store}/starred', [ProductController::class, 'listStoreStarred'])->middleware('auth:sanctum');
Route::get('/products/{store}/starred/{limit}', [ProductController::class, 'listStoreStarred'])->middleware('auth:sanctum');

Route::get('/guest/products/{store}/starred', [ProductController::class, 'listStoreStarred']);
Route::get('/guest/products/{store}/starred/{limit}', [ProductController::class, 'listStoreStarred']);


Route::get('/products/default-category', [ProductController::class, 'listPublicDefaultCategory'])->middleware('auth:sanctum');
Route::get('/products/default-category/{limit}', [ProductController::class, 'listPublicDefaultCategory'])->middleware('auth:sanctum');

Route::get('/guest/products/default-category', [ProductController::class, 'listPublicDefaultCategory']);
Route::get('/guest/products/default-category/{limit}', [ProductController::class, 'listPublicDefaultCategory']);

Route::post('/products/favorite', [ProductController::class, 'addToFavorite'])->middleware('auth:sanctum');
Route::get('/products/favorites', [ProductController::class, 'listFavorites'])->middleware('auth:sanctum');
Route::get('/products/favorites/{limit}', [ProductController::class, 'listFavorites'])->middleware('auth:sanctum');

Route::get('/products/similars/{id}', [ProductController::class, 'listSimilars'])->middleware('auth:sanctum');
Route::get('/guest/products/similars/{id}', [ProductController::class, 'listSimilars']);

Route::get('/products/similars/{id}/{limit}', [ProductController::class, 'listSimilars'])->middleware('auth:sanctum');
Route::get('/guest/products/similars/{id}/{limit}', [ProductController::class, 'listSimilars']);

Route::get('/products/{id}', [ProductController::class, 'find'])->middleware('auth:sanctum');
Route::get('/guest/products/{id}', [ProductController::class, 'find']);

Route::get('/products/search/{query}', [ProductController::class, 'search'])->middleware('auth:sanctum');
Route::get('/products/search/{store}/{query}', [ProductController::class, 'storeSearch'])->middleware('auth:sanctum');
Route::get('/products/search/{store}/{query}/{category}', [ProductController::class, 'storeCategorySearch'])->middleware('auth:sanctum');
Route::get('/products/search/{store}/{query}/{category}/limit/{limit}', [ProductController::class, 'storeCategorySearch'])->middleware('auth:sanctum');
Route::get('/products/search/{store}/{query}/limit/{limit}', [ProductController::class, 'storeSearch'])->middleware('auth:sanctum');

Route::get('/guest/products/search/{query}', [ProductController::class, 'search']);
Route::get('/guest/products/search/{store}/{query}', [ProductController::class, 'storeSearch']);
Route::get('/guest/products/search/{store}/{query}/{category}', [ProductController::class, 'storeCategorySearch']);
Route::get('/guest/products/search/{store}/{query}/{category}/limit/{limit}', [ProductController::class, 'storeCategorySearch']);
Route::get('/guest/products/search/{store}/{query}/limit/{limit}', [ProductController::class, 'storeSearch']);

Route::post('/products/question', [QuestionsAndAnswersController::class, 'question'])->middleware('auth:sanctum');
Route::post('/products/answer', [QuestionsAndAnswersController::class, 'answer'])->middleware('auth:sanctum');
Route::post('/products/question/delete', [QuestionsAndAnswersController::class, 'delete'])->middleware('auth:sanctum');



//-----------------------------------------------------------------------------------------
//  PRODUCTS PUBLIC ACCESS
//-----------------------------------------------------------------------------------------

Route::get('/categories/list', [CategoriesController::class, 'list']);


//-----------------------------------------------------------------------------------------
//  STORE
//-----------------------------------------------------------------------------------------

Route::post('/stores/update', [StoreController::class, 'update'])->middleware('auth:sanctum');
Route::post('/stores/image/update', [StoreSettingController::class, 'setStoreImage'])->middleware('auth:sanctum');
Route::post('/stores/categories/update', [StoreSettingController::class, 'setStoreCategory'])->middleware('auth:sanctum');

Route::get('/stores/images/{store}', [StoreSettingController::class, 'getStoreImages'])->middleware('auth:sanctum');
Route::get('/guest/stores/images/{store}', [StoreSettingController::class, 'getStoreImages']);

Route::get('/stores/categories/{store}', [StoreSettingController::class, 'getStoreCategories'])->middleware('auth:sanctum');
Route::get('/guest/stores/categories/{store}', [StoreSettingController::class, 'getStoreCategories']);

Route::get('/stores/list', [StoreController::class, 'list'])->middleware('auth:sanctum');
Route::get('/stores/questions/{store}', [QuestionsAndAnswersController::class, 'questions'])->middleware('auth:sanctum');

Route::get('/stores/{store}', [StoreController::class, 'find'])->middleware('auth:sanctum');
Route::get('/guest/stores/{store}', [StoreController::class, 'find']);

Route::get('/stores/orders/{store}', [StoreController::class, 'getStoreOrders'])->middleware('auth:sanctum');
Route::get('/slides/list', [StoreController::class, 'listSlides']);

//-----------------------------------------------------------------------------------------
//  PAYMENT METHODS
//-----------------------------------------------------------------------------------------    

Route::get('/payment-methods', [PaymentMethodController::class, 'list'])->middleware('auth:sanctum');

//-----------------------------------------------------------------------------------------
//  ORDERS
//-----------------------------------------------------------------------------------------

Route::post('/orders/create', [OrderController::class, 'store'])->middleware('auth:sanctum');
Route::get('/orders/mine', [OrderController::class, 'mine'])->middleware('auth:sanctum');
Route::post('/orders/qualificate', [OrderController::class, 'performQualification'])->middleware('auth:sanctum');
Route::get('/orders/find/{id}', [OrderController::class, 'find'])->middleware('auth:sanctum');
Route::get('/orders/{store}', [OrderController::class, 'list'])->middleware('auth:sanctum');
Route::post('/orders/status/update', [OrderController::class, 'updateStatus'])->middleware('auth:sanctum');
Route::get('/coupons/available', [OrderController::class, 'getAvailableCoupons'])->middleware('auth:sanctum');

//-----------------------------------------------------------------------------------------
//  USER
//-----------------------------------------------------------------------------------------
Route::post('/users/update', [UserController::class, 'update'])->middleware('auth:sanctum');
Route::post('/users/push', [UserController::class, 'pushMessage'])->middleware('auth:sanctum');
Route::post('/users/office/hide', [UserController::class, 'hideOffice'])->middleware('auth:sanctum');

//-----------------------------------------------------------------------------------------
//  USER PUBLIC ACCESS
//-----------------------------------------------------------------------------------------

Route::get('/institutes/list', [InstitutesController::class, 'list']);
Route::get('/institutes/list/{institute_type}', [InstitutesController::class, 'list']);


//-----------------------------------------------------------------------------------------
//  PLATFORM SETTINGS
//-----------------------------------------------------------------------------------------

Route::get('/platform/exchange-rate', [ExchangeRateController::class, 'getCurrentExchange']);


//-----------------------------------------------------------------------------------------
//  PATIENTS
//-----------------------------------------------------------------------------------------
Route::post('/patients/register', [PatientController::class, 'registerPatient'])->middleware('auth:sanctum');
Route::get('/patients/search/{query}', [PatientController::class, 'patientSearch'])->middleware('auth:sanctum');
Route::get('/patients/mine', [PatientController::class, 'myPatients'])->middleware('auth:sanctum');
Route::post('/patients/information/update', [PatientController::class, 'updatePatient'])->middleware('auth:sanctum');

//-----------------------------------------------------------------------------------------
//  APPOINTMENT
//-----------------------------------------------------------------------------------------

Route::post('/appointments/create', [AppointmentsController::class, 'createAppointment'])->middleware('auth:sanctum');
Route::get('/appointments/mine', [AppointmentsController::class, 'myAppointments'])->middleware('auth:sanctum');
Route::get('/appointments/mine/today', [AppointmentsController::class, 'todayAppointments'])->middleware('auth:sanctum');
Route::post('/appointments/remove', [AppointmentsController::class, 'removeAppointment'])->middleware('auth:sanctum');

//-----------------------------------------------------------------------------------------
//  TREATMENT
//-----------------------------------------------------------------------------------------

Route::post('/patients/treatments/create', [PatientController::class, 'addTreatment'])->middleware('auth:sanctum');
Route::post('/patients/treatments/hide', [PatientController::class, 'hideTreatment'])->middleware('auth:sanctum');

Route::post('/history/familiar/create', [PatientController::class, 'addFamilyHistory'])->middleware('auth:sanctum');
Route::post('/history/familiar/remove', [PatientController::class, 'removeFamilyHistory'])->middleware('auth:sanctum');

Route::post('/history/personal/update', [PatientController::class, 'updatePersonalHistory'])->middleware('auth:sanctum');

// Route::post('/history/personal/update', [PatientController::class, 'updatePersonalHistory'])->middleware('auth:sanctum');