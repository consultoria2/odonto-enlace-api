<?php

use Illuminate\Support\Facades\Route;

use App\Notifications\VerifyUser;
use App\Models\User;
// use App\Http\Controllers\StoreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->away(env('HOME_URL'));
    // return view('welcome');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::get('/verified', function () {
    return view('verified');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('stores', StoreController::class);
    Route::resource('payment-methods', PaymentMethodController::class);
    Route::resource('exchange-rates', ExchangeRateController::class);
    Route::resource('orders', OrderController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('slides', SlideController::class);
    Route::resource('institutes', InstituteController::class);

//-----------------------------------------------------------------------------------------
//  STORE
//-----------------------------------------------------------------------------------------    
    Route::get('/stores/suspend/{id}', [App\Http\Controllers\StoreController::class, 'suspend'])->name('stores.suspend');
    Route::get('/stores/activate/{id}', [App\Http\Controllers\StoreController::class, 'activate'])->name('stores.activate');
    Route::get('/stores/hide/{id}', [App\Http\Controllers\StoreController::class, 'hide'])->name('stores.hide');
    Route::get('/stores/show/{id}', [App\Http\Controllers\StoreController::class, 'show'])->name('stores.show');

//-----------------------------------------------------------------------------------------
//  PAYMENT METHODS
//-----------------------------------------------------------------------------------------    
// Route::get('/payment-methods/edit/{id}', [App\Http\Controllers\PaymentMethodController::class, 'suspend'])->name('payment-methods.edit');
Route::get('/payment-methods/suspend/{id}', [App\Http\Controllers\PaymentMethodController::class, 'suspend'])->name('payment-methods.suspend');
Route::get('/payment-methods/activate/{id}', [App\Http\Controllers\PaymentMethodController::class, 'activate'])->name('payment-methods.activate');


//-----------------------------------------------------------------------------------------
//  ORDERS
//-----------------------------------------------------------------------------------------    
Route::get('/order/approve/{id}', [App\Http\Controllers\OrderController::class, 'approve'])->name('order.approve');
Route::get('/order/cancel/{id}', [App\Http\Controllers\OrderController::class, 'cancel'])->name('order.cancel');
Route::get('/order/deliver/{id}', [App\Http\Controllers\OrderController::class, 'deliver'])->name('order.deliver');
Route::get('/order/delivered/{id}', [App\Http\Controllers\OrderController::class, 'delivered'])->name('order.delivered');

//-----------------------------------------------------------------------------------------
//  EXCHANGE RATE
//-----------------------------------------------------------------------------------------    
// Route::get('/payment-methods/edit/{id}', [App\Http\Controllers\PaymentMethodController::class, 'suspend'])->name('payment-methods.edit');
// Route::get('/exchange/suspend/{id}', [App\Http\Controllers\PaymentMethodController::class, 'suspend'])->name('payment-methods.suspend');
// Route::get('/payment-methods/activate/{id}', [App\Http\Controllers\PaymentMethodController::class, 'activate'])->name('payment-methods.activate');
Route::get('/exchanges/fixate/{id}', [App\Http\Controllers\ExchangeRateController::class, 'fixate'])->name('exchange-rates.fixate');
Route::get('/exchanges/release/{id}', [App\Http\Controllers\ExchangeRateController::class, 'release'])->name('exchange-rates.release');
Route::get('/exchanges/refresh', [App\Http\Controllers\ExchangeRateController::class, 'refresh'])->name('exchange-rates.refresh');

//-----------------------------------------------------------------------------------------
//  SLIDES
//-----------------------------------------------------------------------------------------    
Route::get('/slides/suspend/{id}', [App\Http\Controllers\SlideController::class, 'suspend'])->name('slides.suspend');
Route::get('/slides/activate/{id}', [App\Http\Controllers\SlideController::class, 'activate'])->name('slides.activate');

//-----------------------------------------------------------------------------------------
//  INSTITUTES
//-----------------------------------------------------------------------------------------    
Route::get('/institutes/suspend/{id}', [App\Http\Controllers\InstituteController::class, 'suspend'])->name('institutes.suspend');
Route::get('/institutes/activate/{id}', [App\Http\Controllers\InstituteController::class, 'activate'])->name('institutes.activate');

});






Route::get('/notification/verify-user', function () {
    $user = User::all()->first();

    // Mail::to('consultoria@iideas.biz')->send(new VerifyUser($user));
    $user->notify(new VerifyUser($user));

    return (new VerifyUser($user))
                ->toMail($user);
});
