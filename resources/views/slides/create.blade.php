@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Crear Nuevo Slide</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('slides.index') }}"> Regresar</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif


{!! Form::open(array('route' => 'slides.store','method'=>'POST', 'files' => true)) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre del Slide:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Nombre del Slide, se emplea para SEO','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Descripción del Slide:</strong>
            {!! Form::text('description', null, array('placeholder' => 'Descripción del Slide, se emplea para SEO','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Imagen del Slide:</strong>
            <small>La imagen debe ser de proporción 16:9. Se optimizará para su publicación. Una proporción distinta afectará la UI de la aplicación.</small>
            {!! Form::file('slide_image', ['accept'=>"image/*"]) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tienda:</strong>
            <small>La tienda a donde redirigir al hacer clic sobre el Slide. Solo se muestran tiendas activas.</small>
            {!! Form::select('store_id', $stores, null, ['placeholder' => 'Elije una tienda...']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Crear</button>
    </div>
</div>
{!! Form::close() !!}


@endsection