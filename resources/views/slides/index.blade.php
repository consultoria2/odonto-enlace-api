@extends('layouts.app')


@section('content')
<div class="row card-body">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Administración de Slides</h2>
        </div>
        <div class="pull-right">
            @can('role-create')
            <a class="btn btn-success" href="{{ route('slides.create') }}"> Crear Nuevo Slide</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>Slide #</th>
                        <th>Nombre / Descripción</th>
                        <th>Vista Previa</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($slides as $key => $slide)
                    <tr>
                        <td>
                            {{ $slide->priority }}
                        </td>
                        <td>
                            Nombre: {{ $slide->name }}</br>
                            Descripción: {{ $slide->description }}</br>
                        </td>
                        <td>
                            <img class="img-thumbnail" src="{{ $slide->slide_image }}" /></br>
                        </td>
                        <td>
                            @can('role-edit')
                            @if($slide->is_active)
                            <a class="btn btn-danger" href="{{ route('slides.suspend', $slide->id) }}">Suspender</a>
                            @else
                            <a class="btn btn-success" href="{{ route('slides.activate', $slide->id) }}">Activar</a>
                            @endif
                            @endcan
                            @can('role-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['slides.destroy', $slide->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th style="width: 120px"></th>
                        <th style="width: 120px"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection