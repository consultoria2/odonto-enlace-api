@extends('layouts.app')


@section('content')
<div class="row card-body">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Administración de Pedidos</h2>
        </div>
        <div class="pull-right">
            @can('role-create')

            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>Pedido / Fecha</th>
                        <th>Pedido / Tienda</th>
                        <th>Cliente</th>
                        <th>Detalle</th>
                        <th>Dirección de Envio / Comentarios</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $key => $order)
                    <tr @if($order->status == 'received' && \Carbon\Carbon::now()->subDays(1) > $order->created_at) style="background-color:#FD947D" @endif>
                        <td>
                            {{ $order->id }} / {{ $order->created_at }}
                            @if($order->status == 'received' && \Carbon\Carbon::now()->subDays(1) > $order->created_at)
                            <br>
                            Ha transcurrido ya {{\Carbon\Carbon::parse($order->created_at)->diffInDays(\Carbon\Carbon::now()) }} días
                            @endif
                        </td>
                        <td>{{ $order->store_order_number }} / {{ $order->store->store_name }}</td>
                        <td>
                            Nombre: {{ $order->customer_name }} ({{ $order->buyer->name }} {{ $order->buyer->last_name }})</br>
                            Correo: {{ $order->customer_email }}</br>
                            Teléfono: {{ $order->customer_phone }}</br>
                        </td>
                        <td>
                            Moneda: {{ $order->currency }}</br>
                            Renglones: {{ $order->total_lines }}</br>
                            Neto: {{ $order->net_amount }}</br>
                            Descuento: {{ $order->total_discount }}</br>
                            Impuestos: {{ $order->tax_amount }}</br>
                            Total: {{ $order->total_amount }}</br>
                            Tasa del Pedido: {{ $order->used_exchange_rate }}</br>
                            Total VES: {{ $order->used_exchange_rate * $order->total_amount}}</br>
                        </td>
                        <td>
                            Envío: {{ $order->deliver_to == 'specific_address' ? 'En dirección indicada' : 'Retira en tienda' }}</br>
                            Dirección: {{ $order->shipping_address }}
                            {!! $order->comments !!}</br></br>
                            @foreach($order->lines as $line)
                            {{$line->qty}} x {{$line->product->product_name}} @ {{$line->net_price}} = {{$line->net_amount}}</br>
                            @endforeach
                        </td>
                        <td>
                            Estado: {{ $order->status }}</br>
                            @can('role-edit')
                            @if($order->status == 'received')
                            <a class="btn btn-primary" href="{{ route('order.approve', $order->id) }}">Aprobar</a></br>
                            <a class="btn btn-danger" href="{{ route('order.cancel', $order->id) }}">Cancelar</a>
                            @endif
                            @if($order->status == 'approved')
                            <a class="btn btn-primary" href="{{ route('order.deliver', $order->id) }}">Despachar</a></br>
                            <a class="btn btn-danger" href="{{ route('order.cancel', $order->id) }}">Cancelar</a>
                            @endif
                            @if($order->status == 'delivering')
                            <a class="btn btn-primary" href="{{ route('order.delivered', $order->id) }}">Entregada</a></br>
                            <a class="btn btn-danger" href="{{ route('order.cancel', $order->id) }}">Cancelar</a>
                            @endif
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th style="width: 120px"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection