@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Editar Tienda</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('stores.index') }}"> Volver</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif


{!! Form::model($store, ['method' => 'PATCH','route' => ['stores.update', $store->id]]) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre de la Tienda:</strong>
            {!! Form::text('store_name', null, array('placeholder' => 'Nombre de la Tienda','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Subdominio o Slug de la Tienda:</strong>
            <small>Un subdominio o slug es un nombre corto y sin espacios para identificar la tienda en la URL</small>
            {!! Form::text('store_subdomain', null, array('placeholder' => 'Subdominio o Slug','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Descripción de la Tienda:</strong>
            <small>Texto extenso para mostrar en la ficha tecnica de la tienda</small>
            {!! Form::text('store_description', null, array('placeholder' => 'Descripción de la Tienda','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Dirección de la Tienda:</strong>
            {!! Form::text('store_address', null, array('placeholder' => 'Dirección de la Tienda','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Correo de la Tienda:</strong>
            {!! Form::text('store_email', null, array('placeholder' => 'Correo Electrónico de la Tienda','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Teléfono de Contacto:</strong>
            {!! Form::text('store_phone', null, array('placeholder' => 'Teléfono de la Tienda','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Propietario:</strong>
            {!! Form::select('user_id', $users, null, ['placeholder' => 'Elije un Usuario...']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>
{!! Form::close() !!}

@endsection