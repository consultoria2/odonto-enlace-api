@extends('layouts.app')


@section('content')
<div class="row card-body">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Administración de Tiendas</h2>
        </div>
        <div class="pull-right">
            @can('role-create')
            <a class="btn btn-success" href="{{ route('stores.create') }}"> Crear Nueva Tienda</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nombre</th>
                        <th width="280px">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stores as $store)
                    <tr>
                        <td>{{ $store->id }}</td>
                        <td>{{ $store->store_name }}</td>
                        <td>
                            <!-- <a class="btn btn-info" href="{{ route('stores.show',$store->id) }}">Mostrar</a> -->
                            @can('role-edit')
                            <a class="btn btn-primary" href="{{ route('stores.edit',$store->id) }}">Editar</a>
                            @endcan
                            @can('role-delete')
                            @if($store->is_active)
                            <a class="btn btn-danger" href="{{ route('stores.suspend', $store->id) }}">Suspender</a>
                            @else
                            <a class="btn btn-success" href="{{ route('stores.activate', $store->id) }}">Activar</a>
                            @endif
                            @if($store->allow_navigation)
                            <a class="btn btn-danger" href="{{ route('stores.hide', $store->id) }}">Ocultar</a>
                            @else
                            <a class="btn btn-success" href="{{ route('stores.show', $store->id) }}">Mostrar</a>
                            @endif
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th style="width: 120px"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection