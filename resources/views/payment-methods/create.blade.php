@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Crear Forma de Pago</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('payment-methods.index') }}"> Regresar</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif


{!! Form::open(array('route' => 'payment-methods.store','method'=>'POST')) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Moneda de la Forma de Pago:</strong>
            {!! Form::select('currency', Array('USD' => 'USD', 'VES' => 'VES'), null, ['placeholder' => 'Elije una moneda...']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre de la Forma de Pago:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Nombre de la Forma de pago','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Instrucciones de la Forma de Pago:</strong>
            <small>Incluya aca correos, numero de cuentas, o la información que necesite el cliente para realizar el pago con la forma de pago seleccionada</small>
            {!! Form::text('instructions', null, array('placeholder' => 'Instrucciones de Pago','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Crear</button>
    </div>
</div>
{!! Form::close() !!}


@endsection