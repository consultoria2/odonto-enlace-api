@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Administración de Formas de Pago</h2>
        </div>
        <div class="pull-right">
        @can('role-create')
            <!-- <a class="btn btn-success" href="{{ route('payment-methods.create') }}"> Crear Nueva Forma de Pago</a> -->
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif


<table class="table table-bordered">
  <tr>
     <th>No</th>
     <th>Forma de Pago</th>
     <th width="280px">Acción</th>
  </tr>
    @foreach ($payment_methods as $key => $payment_method)
    <tr>
        <td>{{ ++$i }}</td>
        <td><strong>{{ $payment_method->name }} ({{$payment_method->currency}}):</strong> {{$payment_method->instructions}}</td>
        <td>
            @can('role-edit')
                <a class="btn btn-primary" href="{{ route('payment-methods.edit',$payment_method->id) }}">Editar</a>
            @endcan
            @can('role-delete')
            @if($payment_method->is_active)
                <a class="btn btn-danger" href="{{ route('payment-methods.suspend', $payment_method->id) }}">Suspender</a>
            @else
                <a class="btn btn-success" href="{{ route('payment-methods.activate', $payment_method->id) }}">Activar</a>
            @endif
            @endcan
        </td>
    </tr>
    @endforeach
</table>


{!! $payment_methods->render() !!}

@endsection