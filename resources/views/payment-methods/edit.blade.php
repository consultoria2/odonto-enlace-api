@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Editar Forma de Pago</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('payment-methods.index') }}"> Volver</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif


{!! Form::model($payment_method, ['method' => 'PATCH','route' => ['payment-methods.update', $payment_method->id]]) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Moneda:</strong>
            {!! Form::select('currency', Array('USD' => 'USD', 'VES' => 'VES'), null, ['placeholder' => 'Elije una moneda...']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre de la forma de pago:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Nombre de la Forma de Pago','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Instrucciones de la Forma de Pago:</strong>
            <small>Incluya aca correos, numero de cuentas, o la información que necesite el cliente para realizar el pago con la forma de pago seleccionada</small>
            {!! Form::text('instructions', null, array('placeholder' => 'Instrucciones de Pago','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Slug:</strong>
            {!! Form::text('slug', null, array('placeholder' => 'Este es un identificador interno, no cambiarlo','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>
{!! Form::close() !!}

@endsection