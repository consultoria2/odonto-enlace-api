@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Administración de Instituciones</h2>
        </div>
        <div class="pull-right">
        @can('role-create')
            <a class="btn btn-success" href="{{ route('institutes.create') }}"> Crear Nueva Institución</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@elseif ($message = Session::get('error'))
    <div class="alert alert-danger">
        <p>{{ $message }}</p>
    </div>
@endif


<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($institutes as $key => $institute)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $institute->institute_name }}</td>
                        <td>{{ $institute->institute_type }}</td>
                        <td>
                            @can('role-edit')
                                <a class="btn btn-primary" href="{{ route('institutes.edit',$institute->id) }}">Editar</a>
                            @endcan
                            @can('role-delete')
                                @if($institute->is_active)
                                <a class="btn btn-danger" href="{{ route('institutes.suspend', $institute->id) }}">Suspender</a>
                                @else
                                <a class="btn btn-success" href="{{ route('institutes.activate', $institute->id) }}">Activar</a>
                                @endif
                                {!! Form::open(['method' => 'DELETE','route' => ['institutes.destroy', $institute->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                <tfoot>
                    <tr>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th><input type="text" placeholder="Buscar" /></th>
                        <th style="width: 120px"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


@endsection