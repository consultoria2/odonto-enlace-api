@extends('layouts.headless')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('¡Te has verificado!') }}</div>

                <div class="card-body">
                    
                <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a href="odontoenlace://odontoenlace" class="btn btn-primary">{{ __('Continuar a Odontoenlace') }}</a>
                            </div>
                        </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection