@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Administración de Tasas de Cambio</h2>
        </div>
        <div class="pull-right">
            @can('role-create')
            <a class="btn btn-success" href="{{ route('exchange-rates.refresh') }}"> Obtener Tasa Actual</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger">
    <p>{{ $message }}</p>
</div>
@endif

<div class="alert alert-info">
    <p>Importante: La <strong>Tasa Fijada</strong> tiene prioridad sobre la <strong>Tasa Vigente</strong>. Si no existe una <strong>Tasa Fijada</strong>, se emplea entonces la <strong>Tasa Vigente</strong></p>
</div>


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Moneda Base <span>&#8594;</span> Moneda Destino @ Tasa de Conversión</th>
        <th>Status</th>
        <th width="280px">Acción</th>
    </tr>
    @foreach ($exchange_rates as $key => $exchange_rate)
    <tr>
        <td>{{ ++$i }}</td>
        <td>
            {{$exchange_rate->from_currency}}<span>&#8594;</span>{{$exchange_rate->to_currency}} @ {{$exchange_rate->rate}}
            @if($exchange_rate->fixed)
            </br>
            <small><strong>Tasa Fijada</strong></small>
            @endif
            @if(is_null($exchange_rate->until))
            </br>
            <small><strong>Tasa Vigente</strong></small>
            @endif
        </td>
        <td>
            {{$exchange_rate->since}} : {{$exchange_rate->since}} </br>
            Actualizado: {{$exchange_rate->updated_at}}
            @if($exchange_rate->edited)
            </br>
            <small><strong>Tasa Editada</strong></small>
            @endif
        </td>
        <td>
            @can('role-edit')
            <a class="btn btn-primary" href="{{ route('exchange-rates.edit',$exchange_rate->id) }}">Editar</a>
            @endcan
            @can('role-delete')
            @if($exchange_rate->fixed)
            <a class="btn btn-danger" href="{{ route('exchange-rates.release', $exchange_rate->id) }}">Liberar</a>
            @else
            <a class="btn btn-success" href="{{ route('exchange-rates.fixate', $exchange_rate->id) }}">Fijar</a>
            @endif
            @endcan
        </td>
    </tr>
    @endforeach
</table>


{!! $exchange_rates->render() !!}

@endsection