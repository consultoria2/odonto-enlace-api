@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Editar Tasa de Cambio</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('exchange-rates.index') }}"> Volver</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif


{!! Form::model($exchange_rate, ['method' => 'PATCH','route' => ['exchange-rates.update', $exchange_rate->id]]) !!}
<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Valor de la tasa de cambio:</strong>
            {!! Form::number('rate', $exchange_rate->rate, array('placeholder' => 'Tasa de cambio','class' => 'form-control', 'step' => '0.01')) !!}
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>
{!! Form::close() !!}

@endsection