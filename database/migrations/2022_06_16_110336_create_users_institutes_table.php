<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersInstitutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_institutes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('user_id');
            $table->integer('institute_id');
            $table->string('degree_name')->nullable();
            $table->string('degree_title')->nullable();
            $table->string('degree_level')->nullable();
            $table->string('degree_local_number')->nullable();
            $table->string('degree_national_number')->nullable();
            $table->string('degree_international_number')->nullable();
            $table->string('degree_picture')->nullable();
            $table->string('degree_custom_institute')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_institutes');
    }
}
