<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_type')->nullable();
            $table->string('main_image');
            $table->string('product_name');
            $table->string('brand')->nullable();;
            $table->decimal('discount_price', 12, 2)->nullable();
            $table->decimal('price',12, 2);
            $table->smallInteger('avg_rate')->nullable();
            $table->boolean('availability');
            $table->string('product_code');
            $table->text('tags')->nullable();
            $table->text('description');
            $table->text('features')->nullable();
            $table->text('image_gallery')->nullable();
            $table->string('category')->nullable();
            $table->integer('stock');
            $table->unsignedInteger('store_id');
            $table->timestamps();
            $table->index(['store_id', 'product_code'])->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
