<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelativesMedicalHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relatives_medical_history', function (Blueprint $table) {
            $table->id();
            $table->integer('dentist_id');
            $table->integer('patient_id');
            $table->string('family_history_parenthood');
            $table->string('family_history_medical');
            $table->string('family_history_notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
